/* 
 * File:   LinkedList.h
 * Author: Stanley
 *
 * Created on November 28, 2016, 12:47 AM
 */

#ifndef LINKEDLIST_H
#define	LINKEDLIST_H
#include "Node.h"
#include <iostream>

class LinkedList
{
public:
    // Constructor
    LinkedList();
    LinkedList(const LinkedList& orig);
    virtual ~LinkedList();
    
    // Functions
    void append(int val);
    void insert(int val);
    void remove(int val);
    void display();
private:
    Node* head;
};

#endif	/* LINKEDLIST_H */

