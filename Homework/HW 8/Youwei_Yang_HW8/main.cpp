/* 
 * File:   main.cpp
 * Author: Stanley
 *
 * Created on November 28, 2016, 12:45 AM
 */

#include <cstdlib>
#include "LinkedList.h"

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    LinkedList newList;
    
    cout << "Add: 5" << endl;
    newList.insert(5);
    newList.display();
    
    cout << endl << endl << "Add: 7" << endl;
    newList.insert(7);
    newList.display();
    
    cout << endl << endl << "Add: 3" << endl;
    newList.insert(3);
    newList.display();
    
    cout << endl << endl << "Append 2" << endl;
    newList.append(2);
    newList.display();
    
    cout << endl << endl << "Delete 2" << endl;
    newList.remove(2);
    newList.display();
    
    cout << endl << endl << "Delete 5" << endl;
    newList.remove(5);
    newList.display();
    
    return 0;
}

