/* 
 * File:   Node.h
 * Author: Stanley
 *
 * Created on November 28, 2016, 12:45 AM
 */

#ifndef NODE_H
#define	NODE_H
#include <cstdlib>

using namespace std;

class Node
{
public:
    // Constructor
    Node();
    Node(const Node& orig);
    virtual ~Node();
    
    // Public Variables
    int value;
    Node* next;
private:

};

#endif	/* NODE_H */

