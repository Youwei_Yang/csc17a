/* 
 * File:   LinkedList.cpp
 * Author: Stanley
 * 
 * Created on November 28, 2016, 12:47 AM
 */

#include "LinkedList.h"

LinkedList::LinkedList()
{
    head = NULL;
}

LinkedList::LinkedList(const LinkedList& orig)
{
    
}

LinkedList::~LinkedList()
{
    
}

void LinkedList::append(int val)
{
    Node* newNode = new Node;
    newNode->value = val;
    
    // pointers
    Node* nodePtr;
    if(!head)
    {
        head = newNode;
    }
    else
    {
        nodePtr = head;
        while(nodePtr->next)
        {
            nodePtr = nodePtr->next;
        }
        nodePtr->next = newNode;
    }
}

void LinkedList::insert(int val)
{
    Node* newNode = new Node;
    newNode->value = val;
    
    // pointers
    Node* nodePtr;
    Node* prevNode = NULL;
    if(!head)
    {
        head = newNode;
    }
    else
    {
        nodePtr = head;
        while(nodePtr != NULL && nodePtr->value < val)
        {
            prevNode = nodePtr;
            nodePtr = nodePtr->next;
        }
        if(!prevNode)
        {
            newNode->next = nodePtr;
            head = newNode;
        }
        else
        {
            prevNode->next = newNode;
            newNode->next = nodePtr;
        }
    }
}

void LinkedList::remove(int val)
{
    if(!head)
        return;
    
    // pointers
    Node* prevNode = NULL;
    Node* nodePtr = head;
    
    while(nodePtr->value != NULL && nodePtr->value != val)
    {
        prevNode = nodePtr;
        nodePtr = nodePtr->next;
    }
    
    if(!nodePtr)
        return;
    
    if(prevNode)
    {
        Node*temp = nodePtr->next;
        delete nodePtr;
        prevNode->next = temp;
    }
    else
    {
        Node*temp = nodePtr->next;
        delete nodePtr;
        head = temp;
    }
}

void LinkedList::display()
{
    Node*nodePtr = head;
    
    while(nodePtr)
    {
        cout << nodePtr->value << " ";
        nodePtr = nodePtr->next;
    }
}