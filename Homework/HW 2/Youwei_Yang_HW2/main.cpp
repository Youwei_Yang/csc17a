/* 
 * File:   main.cpp
 * Author: Stanley
 *
 * Created on September 9, 2016, 5:07 PM
 */

#include <cstdlib>
#include <iostream>
#include <cstdlib>

using namespace std;

void randNum(int [], int);
void printIntArray(int [], int);
void reverseIntArray(int*&, int);
double intAverage(int [], int);
int intMode(int [], int);
void ascendingSort(int*&, int);
double intMedian(int[], int);
int cstringCount(char []);
void cstringBackward(char*&, int);
void combineName(char*&, char*&, char*&, char*&);
int sumOfCString(char*&);

/*
 * 
 */
int main(int argc, char** argv) {

    //initiate random seed
    srand(time(NULL));
    
    //----------------------------9.10 Reverse Array----------------------------
    cout << "9.10 Reverse Array" << endl;
    int size = 5;
    int*intArray = new int[size];
    randNum(intArray, size);
    cout << "Original int array: ";
    printIntArray(intArray, size);
    reverseIntArray(intArray, size);
    cout << "Reversed int array: ";
    printIntArray(intArray, size);
    
    //---------------------------9.13 Movie Statistics--------------------------
    cout << endl << "9.13 Movie Statistics" << endl;
    cout << "Number of student survey: ";
    cin >> size;
    int*studentSurvey = new int[size];
    cout << "Enter the number of movies saw by following students" << endl;
    for (int i = 0; i < size; i++){
        cout << "student" << i+1 << ": ";
        cin >> studentSurvey[i];
    }
    cout << "Average: " << intAverage(studentSurvey, size) << endl;
    cout << "Mode: " << intMode(studentSurvey, size) << endl;
    ascendingSort(studentSurvey, size);
    cout << "Median: " << intMedian(studentSurvey, size);
    
    //----------------------------10.1 String Length----------------------------
    cout << endl << endl << "10.1 String Length" << endl;
    char newString[100];
    cout << "Enter a string: ";
    cin.clear();
    cin.ignore(1000, 10);
    cin.getline(newString, 100);
    char*newCString = newString;
    int count = cstringCount(newCString);
    cout << "Number of characters: " << count;
    
    //----------------------------10.2 Backward String--------------------------
    cout << endl << endl << "10.2 Backward String" << endl;
    cstringBackward(newCString, count);
    cout << "Reversed String: ";
    for(int i = 0; i < count; i++){
        cout << newCString[i];
    }
    
    //----------------------------10.7 Name Arranger----------------------------
    cout << endl << endl << "10.7 Name Arranger" << endl;
    char fnString[100];
    char mnString[100];
    char lsString[100];
    char fullString[100];
    cout << "First name: ";
    cin.clear();
    cin.getline(fnString, 100);
    char*firstName = fnString;
    cout << "Middle name: ";
    cin.clear();
    cin.getline(mnString, 100);
    char*middleName = mnString;
    cout << "Last name: ";
    cin.clear();
    cin.getline(lsString, 100);
    char*lastName = lsString;
    char*fullName = new char[100];
    combineName(firstName, middleName, lastName, fullName);
    cout << "Full Name: ";
    for(int i = 0; i < cstringCount(fullName); i++){
        cout << fullName[i];
    }
    
    //-----------------------10.8 Sum of digits in string-----------------------
    cout << endl << endl << "10.8 Sum of digits in string" << endl;
    char nString[100];
    cout << "Enter a series of numbers: ";
    cin.clear();
    cin.getline(nString, 100);
    char*numberString = nString;
    cout << "The sum is: " << sumOfCString(numberString);
    
    
    return 0;
}

/**
 * Insert random number to an array
 * @param a The array
 * @param size The size of the array
 */
void randNum(int a[], int size){
    for(int i = 0; i < size; i++){
        a[i] = rand() % 100;
    }
}

/**
 * Print an array of int
 * @param a The array
 * @param size The size of the array
 */
void printIntArray(int a[], int size){
    for(int i = 0; i < size; i++){
        cout << a[i] << " ";
    }
    cout << endl;
}

/**
 * Reverse an int array
 * @param a The array
 * @param size The size of the array
 */
void reverseIntArray(int*&a, int size){
    int*temp = new int[size];
    
    for(int i = 0; i < size; i++){
        temp[size - i - 1] = a[i];
    }
    
    delete[] a;
    a = temp;
}

/**
 * Calculate the average of an int array
 * @param a The array
 * @param size The size of the array
 * @return A double with the average of the array
 */
double intAverage(int a[], int size){
    double total = 0;
    
    for(int i = 0; i < size; i++){
        total += a[i];
    }
    
    return total/size;
}

/**
 * Calculates the mode of an int array
 * @param a The array
 * @param size The size of the array
 * @return An int with the mode of the array. If there exist no mode, return -1
 */
int intMode(int a[], int size){
    int mode = -1;
    int apperence1 = 1;
    int apperence2 = 1;
    
    for(int i = 0; i < size; i ++){
        for(int j = 1; j < size; j ++){
            if(a[i] == a[j]){
                apperence1 ++;
            }
        }
        if(apperence1 > apperence2){
            apperence2 = apperence1;
            mode = a[i];
        }
        apperence1 = 0;
    }
    
    return mode;
}

/**
 * Sort an dynamic array in ascending order
 * @param array The array
 * @param size The size of the array
 * @return A sorted array
 */
 void ascendingSort(int*&array, int size){
    int a;
    for(int i = 0; i < size; i++){
        for(int j = 0; j < size; j++){
            if( array[i] < array[j]){
                a = array[i];
                array[i] = array[j];
                array[j] = a;
            }
        }
    }
}

/**
 * Calculate the median of an int array
 * @param a The array
 * @param size The size of the array
 * @return A double with the median of the array
 */
double intMedian(int a[], int size){
    double median;
    int middle = size/2;
    
    if(size%2 == 0){ //even number of elements
        median += a[middle];
        median += a[middle - 1];
        median = median/2;
    } else { //odd number of elements
        median = a[middle];
    }
    
    return median;
}

/**
 * Count the number of characters in a C-String
 * @param c The C-String
 * @return An int of number of characters
 */
int cstringCount(char c[]){
    int count = 0;
    while(c[count] != '\0'){
        count++;
    }
    
    return count;
}

/**
 * Reverse the C-String
 * @param c The C-String
 */
void cstringBackward(char*&c, int size){
    char*CString = new char[100];
    int count = 0;
    
    for(int i = size; i > 0; i--){
        CString[count] = c[i-1];
        count++;
    }
    
    c = CString;
}

/**
 * Combines the three C-String into one C-String
 * @param fn First name C-String
 * @param mn Middle Name C-String
 * @param ln Last Name C-String
 * @param full Full Name C-String
 */
void combineName(char*&fn, char*&mn, char*&ln, char*&full){
    char*temp = new char[100];
    int count = 0;
    
    for(int i = 0; i < cstringCount(ln); i++){
        temp[count] = ln[i];
        count++;
    }
    temp[count] = ',';
    count++;
    temp[count] = ' ';
    count++;
    
    for(int i = 0; i < cstringCount(fn); i++){
        temp[count] = fn[i];
        count++;
    }
    temp[count] = ' ';
    count++;
    
    for(int i = 0; i < cstringCount(mn); i++){
        temp[count] = mn[i];
        count++;
    }
    
    delete[] full;
    full = temp;
}

/**
 * Calculates all the numbers within a C-String
 * @param n The C-String
 * @return An int of the result
 */
int sumOfCString(char*&n){
    int result = 0;
    char*temp = new char[1];
    
    for(int i = 0; i < cstringCount(n); i++){
        temp[0] = n[i];
        result += atoi(temp);
    }
    
    return result;
}
