/* 
 * File:   Employee.cpp
 * Author: Stanley
 * 
 * Created on November 11, 2016, 2:50 PM
 */

#include "Employee.h"

Employee::Employee()
{
    name = "";
    number = 00000;
    hireDate = "00/00/0000";
}

Employee::Employee(string name, int number, string hireDate)
{
    this->name = name;
    this->number = number;
    this->hireDate = hireDate;
}

Employee::Employee(const Employee& orig)
{
    this->name = orig.name;
    this->number = orig.number;
    this->hireDate = orig.hireDate;
}

Employee::~Employee()
{
    
}

void Employee::setName(string name)
{
    this->name = name;
}

void Employee::setNumber(int number)
{
    this->number = number;
}

void Employee::setHireDate(string hireDate)
{
    this->hireDate = hireDate;
}

string Employee::getName()
{
    return name;
}

int Employee::getNumber()
{
    return number;
}

string Employee::getHireDate()
{
    return hireDate;
}

void Employee::print()
{
    cout << "Employee Name: " << name << endl;
    cout << "Employee Number: " << number << endl;
    cout << "Employee Hire Date: " << hireDate << endl;
}