/* 
 * File:   ShiftSupervisor.cpp
 * Author: Stanley
 * 
 * Created on November 11, 2016, 4:13 PM
 */

#include "ShiftSupervisor.h"

ShiftSupervisor::ShiftSupervisor()
{
    annualSalary = 0.00;
    annualBonus = 0.00;
}

ShiftSupervisor::ShiftSupervisor(double annualSalary, double annualBonus, string name, int number, string hireDate)
    : Employee(name, number, hireDate)
{
    this->annualSalary = annualSalary;
    this->annualBonus = annualBonus;
}

ShiftSupervisor::ShiftSupervisor(const ShiftSupervisor& orig)
    : Employee(orig)
{
    this->annualSalary = orig.annualSalary;
    this->annualBonus = orig.annualBonus;
}

ShiftSupervisor::~ShiftSupervisor()
{
    
}

void ShiftSupervisor::setAnnualSalary(double annualSalary)
{
    this->annualSalary = annualSalary;
}

void ShiftSupervisor::setAnnualBonus(double annualBonus)
{
    this->annualBonus = annualBonus;
}

double ShiftSupervisor::getAnnualSalary()
{
    return annualSalary;
}

double ShiftSupervisor::getAnnualBonus()
{
    return annualBonus;
}

void ShiftSupervisor::print()
{
    Employee::print();
    cout << "Annual Salary: " << annualSalary << endl;
    cout << "Annual Bonus: " << annualBonus << endl;
}