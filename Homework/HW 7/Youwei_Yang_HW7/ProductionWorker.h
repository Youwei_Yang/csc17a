/* 
 * File:   ProductionWorker.h
 * Author: Stanley
 *
 * Created on November 11, 2016, 2:59 PM
 */

#ifndef PRODUCTIONWORKER_H
#define	PRODUCTIONWORKER_H

#include "Employee.h"

class ProductionWorker : public Employee
{
public:
    // Constructor
    ProductionWorker();
    /**
     * Create a ProductionWorker object with complete information
     * @param shift an integer 1 represents day shift, while 2 represents night shift
     * @param payRate hourly pay rate
     */
    ProductionWorker(int shift, double payRate, string name, int number, string hireDate);
    ProductionWorker(const ProductionWorker& orig);
    virtual ~ProductionWorker();
    // Getters && Setters
    void setShift(int shift);
    void setPayRate(double payRate);
    int getShift();
    double getPayRate();
    void print();
private:
    int shift;
    double payRate;
};

#endif	/* PRODUCTIONWORKER_H */

