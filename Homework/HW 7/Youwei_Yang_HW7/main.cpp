/* 
 * File:   main.cpp
 * Author: Stanley
 *
 * Created on November 11, 2016, 2:38 PM
 */

#include <cstdlib>
#include <iostream>
#include "ProductionWorker.h"
#include "ShiftSupervisor.h"
#include "TeamLeader.h"

using namespace std;

void line();

/*
 * 
 */
int main(int argc, char** argv) {

    bool run = true;
    int menuInput;
    
    while(run)
    {
        cout << "HW 7" << endl << endl
                << "1) 15.1 Employee and ProductionWorker Classes" << endl
                << "2) 15.2 ShiftSupervisor Class" << endl
                << "3) 15.3 TeamLeader Class" << endl
                << "4) Exit" << endl
                << "input: ";
        cin >> menuInput;
        
        line();
        
        if(menuInput < 1 || menuInput > 4)
        {
            cout << "ERROR: Invalid Input" << endl;
            break;
        }
        
        switch(menuInput)
        {
            case 1: // 15.1 Employee and ProductionWorker Classes
            {
                cout << "15.1 Employee and ProductionWorker Classes" << endl << endl;
                
                cout << "Create a new ProductionWorker object" << endl;
                ProductionWorker worker1(1, 16.99, "Stanley", 123456, "01/01/2016");
                worker1.print();
                
                cout << endl << "Create a copy of the ProductionWorker object" << endl;
                ProductionWorker worker2(worker1);
                worker2.print();
                
                break;
            }
            
            case 2:
            {
                cout << "15.2 ShiftSupervisor Class" << endl << endl;
                
                cout << "Create a new ShiftSupervisor object" << endl;
                ShiftSupervisor supervisor1(100000, 5000, "Leanne", 987654, "12/12/2016");
                supervisor1.print();
                
                cout << endl << "Increase the annual bonus to 10000" << endl;
                supervisor1.setAnnualBonus(10000);
                supervisor1.print();
                
                break;
            }
            
            case 3:
            {
                cout << "15.3 TeamLeader Class" << endl << endl;
                
                cout << "Create a new TeamLeader object" << endl;
                TeamLeader leader1(500, 15.0, 0.0, 2, 25.50, "Amanda", 465823, "05/05/2016");
                leader1.print();
                
                cout << endl << "Add 2 hour and 30 minutes to the attendedTrainingHour" << endl;
                leader1.setAttendedTrainingHour(2.5);
                leader1.print();
                
                cout << endl << "Create a copy of TeamLeader object" << endl;
                TeamLeader leader2(leader1);
                leader2.print();
                
                break;
            }
            
            case 4:
            {
                cout << "Exiting the program" << endl;
                
                exit(0);
            }
        }
        
        line();
        
    }
    
    return 0;
}

void line()
{
    for(int i = 0; i < 35; i++)
    {
        cout << "-";
    }
    
    cout << endl;
}