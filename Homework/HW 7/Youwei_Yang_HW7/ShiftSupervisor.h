/* 
 * File:   ShiftSupervisor.h
 * Author: Stanley
 *
 * Created on November 11, 2016, 4:13 PM
 */

#ifndef SHIFTSUPERVISOR_H
#define	SHIFTSUPERVISOR_H
#include "Employee.h"

class ShiftSupervisor : public Employee
{
public:
    // Constructor
    ShiftSupervisor();
    ShiftSupervisor(double annualSalary, double annualBonus, string name, int number, string hireDate);
    ShiftSupervisor(const ShiftSupervisor& orig);
    virtual ~ShiftSupervisor();
    // Getters && Setters
    void setAnnualSalary(double annualSalary);
    void setAnnualBonus(double annualBonus);
    double getAnnualSalary();
    double getAnnualBonus();
    void print();
private:
    double annualSalary;
    double annualBonus;
};

#endif	/* SHIFTSUPERVISOR_H */

