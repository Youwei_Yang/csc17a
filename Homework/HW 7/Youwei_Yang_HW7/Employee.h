/* 
 * File:   Employee.h
 * Author: Stanley
 *
 * Created on November 11, 2016, 2:50 PM
 */

#ifndef EMPLOYEE_H
#define	EMPLOYEE_H

#include <iostream>

using namespace std;

class Employee
{
public:
    // Constructor
    Employee();
    Employee(string name, int number, string hireDate);
    Employee(const Employee& orig);
    virtual ~Employee();
    
    // Getters && Setters
    void setName(string name);
    void setNumber(int number);
    void setHireDate(string hireDate);
    string getName();
    int getNumber();
    string getHireDate();
    void print();
    
private:
    string name;
    int number;
    string hireDate;
};

#endif	/* EMPLOYEE_H */

