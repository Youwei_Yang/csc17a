/* 
 * File:   ProductionWorker.cpp
 * Author: Stanley
 * 
 * Created on November 11, 2016, 2:59 PM
 */

#include "ProductionWorker.h"

ProductionWorker::ProductionWorker()
    : Employee()
{
    shift = 0;
    payRate = 0.00;
}

ProductionWorker::ProductionWorker(int shift, double payRate, string name, int number, string hireDate)
    : Employee(name, number, hireDate)
{
    this->shift = shift;
    this->payRate = payRate;
}

ProductionWorker::ProductionWorker(const ProductionWorker& orig)
    : Employee(orig)
{
    this->shift = orig.shift;
    this->payRate = orig.payRate;
}

ProductionWorker::~ProductionWorker()
{
    
}

void ProductionWorker::setShift(int shift)
{
    this->shift = shift;
}

void ProductionWorker::setPayRate(double payRate)
{
    this->payRate = payRate;
}

int ProductionWorker::getShift()
{
    return shift;
}

double ProductionWorker::getPayRate()
{
    return payRate;
}

void ProductionWorker::print()
{
    Employee::print();
    cout << "Shift: " << shift << endl;
    cout << "Pay Rate: " << payRate << endl;
}