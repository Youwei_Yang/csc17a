/* 
 * File:   TeamLeader.h
 * Author: Stanley
 *
 * Created on November 11, 2016, 4:30 PM
 */

#ifndef TEAMLEADER_H
#define	TEAMLEADER_H
#include "ProductionWorker.h"

class TeamLeader : public ProductionWorker
{
public:
    // Constructor
    TeamLeader();
    TeamLeader(double monthlyBonus, double requiredTrainingHour, double attendedTrainingHour,
            int shift, double payRate, string name, int number, string hireDate);
    TeamLeader(const TeamLeader& orig);
    virtual ~TeamLeader();
    // Getters && Setters
    void setMonthlyBonus(double monthlyBonus);
    void setRequiredTrainingHour(double requiredTrainingHour);
    void setAttendedTrainingHour(double attendedTrainingHour);
    double getMonthlyBonus();
    double getRequiredTrainingHour();
    double getAttendedTrainingHour();
    void print();
private:
    double monthlyBonus;
    double requiredTrainingHour;
    double attendedTrainingHour;
};

#endif	/* TEAMLEADER_H */

