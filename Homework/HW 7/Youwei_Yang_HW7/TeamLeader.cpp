/* 
 * File:   TeamLeader.cpp
 * Author: Stanley
 * 
 * Created on November 11, 2016, 4:30 PM
 */

#include "TeamLeader.h"

TeamLeader::TeamLeader()
{
    monthlyBonus = 0.00;
    requiredTrainingHour = 0;
    attendedTrainingHour = 0;
}

TeamLeader::TeamLeader(double monthlyBonus, double requiredTrainingHour, double attendedTrainingHour,
        int shift, double payRate, string name, int number, string hireDate)
        : ProductionWorker(shift, payRate, name, number, hireDate)
{
    this->monthlyBonus = monthlyBonus;
    this->requiredTrainingHour = requiredTrainingHour;
    this->attendedTrainingHour = attendedTrainingHour;
}

TeamLeader::TeamLeader(const TeamLeader& orig)
    : ProductionWorker(orig)
{
    this->monthlyBonus = orig.monthlyBonus;
    this->requiredTrainingHour = orig.requiredTrainingHour;
    this->attendedTrainingHour = orig.attendedTrainingHour;
}

TeamLeader::~TeamLeader()
{
    
}

void TeamLeader::setMonthlyBonus(double monthlyBonus)
{
    this->monthlyBonus = monthlyBonus;
}

void TeamLeader::setRequiredTrainingHour(double requiredTrainingHour)
{
    this->requiredTrainingHour = requiredTrainingHour;
}

void TeamLeader::setAttendedTrainingHour(double attendedTrainingHour)
{
    this->attendedTrainingHour = attendedTrainingHour;
}

double TeamLeader::getMonthlyBonus()
{
    return monthlyBonus;
}

double TeamLeader::getRequiredTrainingHour()
{
    return requiredTrainingHour;
}

double TeamLeader::getAttendedTrainingHour()
{
    return attendedTrainingHour;
}

void TeamLeader::print()
{
    ProductionWorker::print();
    cout << "Monthly Bonus: " << monthlyBonus << endl;
    cout << "Required Training Hour: " << requiredTrainingHour << endl;
    cout << "Attended Training Hour: " << attendedTrainingHour << endl;
}