/* 
 * File:   DynamicQueue.h
 * Author: Stanley
 *
 * Created on December 4, 2016, 6:40 PM
 */

#ifndef DYNAMICQUEUE_H
#define	DYNAMICQUEUE_H
#include "NodeTemplate.h"
#include <cstdlib>
#include <iostream>

using namespace std;

template <class T>
class DynamicQueue
{
public:
    // Constructors
    DynamicQueue();
    DynamicQueue(const DynamicQueue& orig);
    virtual ~DynamicQueue();
    
    // Other Functions
    void enqueue(T x);
    void dequeue(T&x);
    void clear();
    bool isEmpty();
private:
    Node<T>* front;
    Node<T>* rear;
    int numItems;
};

template <class T>
DynamicQueue<T>::DynamicQueue()
{
    front = rear = NULL;
    numItems = 0;
}

template <class T>
DynamicQueue<T>::DynamicQueue(const DynamicQueue& orig)
{
    
}

template <class T>
DynamicQueue<T>::~DynamicQueue()
{
    
}

template <class T>
void DynamicQueue<T>::enqueue(T x)
{
    try{
        Node<T>* newNode = new Node<T>();
        newNode->val = x;
        newNode->tail = NULL;
        if(isEmpty())
        {
            front = rear = newNode;
        }
        else
        {
            rear->tail = newNode;
            rear = newNode;
        }
        numItems++;
    }
    catch(bad_alloc)
    {
        cout << "ERROR: Cannot allocate memory" << endl;
        exit(EXIT_FAILURE);
    }
}

template <class T>
void DynamicQueue<T>::dequeue(T& x)
{
    if(isEmpty())
        cout << "ERROR: Queue is empty" << endl;
    else
    {
        x = front->val;
        Node<T>* temp = front->tail;
        delete front;
        front = temp;
        numItems--;
    }
}

template <class T>
void DynamicQueue<T>::clear()
{
    T x;
    
    for(int i = 0; i < numItems; i++)
        dequeue(x);
}

template <class T>
bool DynamicQueue<T>::isEmpty()
{
    return numItems <= 0;
}

#endif	/* DYNAMICQUEUE_H */

