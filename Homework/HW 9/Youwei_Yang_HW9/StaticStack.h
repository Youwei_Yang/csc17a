/* 
 * File:   StaticStack.h
 * Author: Stanley
 *
 * Created on December 2, 2016, 4:38 PM
 */

#ifndef STATICSTACK_H
#define	STATICSTACK_H
#include <cstdlib>
#include <iostream>

using namespace std;

template <class T>
class StaticStack
{
public:
    // Constructor
    StaticStack(T size);
    StaticStack(const StaticStack& orig);
    virtual ~StaticStack();
    // Other Functions
    void push(T x);
    void pop(T&x);
    void print();
    
private:
    T* stack;
    int size;
    T top;
    bool isEmpty();
    bool isFull();
};

template <class T>
StaticStack<T>::StaticStack(T size)
{
    this->size = size;
    stack = new T[size];
    top = -1;
}

template <class T>
StaticStack<T>::StaticStack(const StaticStack& orig)
{
    size = orig.size;
    stack = new T[size];
    for(int i = 0; i < size; i++)
    {
        stack[i] = orig.stack[i];
    }
    top = orig.top;
}

template <class T>
StaticStack<T>::~StaticStack()
{
    delete[] stack;
}

template <class T>
bool StaticStack<T>::isFull()
{
    return top == size -1;
}

template <class T>
bool StaticStack<T>::isEmpty()
{
    return top == -1;
}

template <class T>
void StaticStack<T>::push(T x)
{
    if(isFull())
    {
        cout << "ERROR: STACK IS FULL" << endl;
    }
    else
    {
        top++;
        stack[top] = x;
    }
}

template <class T>
void StaticStack<T>::pop(T& x)
{
    if(isEmpty())
    {
        cout << "ERROR: STACK IS EMPTY" << endl;
    }
    else
    {
        x = stack[top];
        top--;
    }
}

template <class T>
void StaticStack<T>::print()
{
    if(isEmpty())
    {
        cout << "ERROR: STACK IS EMPTY" << endl;
    }
    else
    {
        for(int i = 0; i < (top + 1); i++)
        {
            cout << stack[i] << " ";
        }
        cout << endl;
    }
}

#endif	/* STATICSTACK_H */

