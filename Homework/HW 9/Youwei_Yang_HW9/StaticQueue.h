/* 
 * File:   StaticQueue.h
 * Author: Stanley
 *
 * Created on December 4, 2016, 5:56 PM
 */

#ifndef STATICQUEUE_H
#define	STATICQUEUE_H
#include <cstdlib>
#include <iostream>

using namespace std;

template <class T>
class StaticQueue {
public:
    // Constructors
    StaticQueue(int size);
    StaticQueue(const StaticQueue& orig);
    virtual ~StaticQueue();
    
    // Other Functions
    void enqueue(T x);
    void dequeue(T&x);
    void clear();
    bool isEmpty();
    bool isFull();
private:
    T* qArray;
    int size;
    int front;
    int rear;
    int numItems;
};

template <class T>
StaticQueue<T>::StaticQueue(int size)
{
    this->size = size;
    qArray = new T[size];
    front = rear = -1;
    numItems = 0;
}

template <class T>
StaticQueue<T>::StaticQueue(const StaticQueue& orig)
{
    
}

template <class T>
StaticQueue<T>::~StaticQueue()
{
    
}

template <class T>
void StaticQueue<T>::enqueue(T x)
{
    if(isFull())
        cout << "ERROR: Queue is full" << endl;
    else
    {
        rear = (rear + 1) % size;
        qArray[rear] = x;
        numItems++;
    }
}

template <class T>
void StaticQueue<T>::dequeue(T& x)
{
    if(isEmpty())
        cout << "ERROR: Queue is empty" << endl;
    else
    {
        front = (front + 1) % size;
        x = qArray[front];
        numItems--;
    }
}

template <class T>
void StaticQueue<T>::clear()
{
    T x;
    for(int i = 0; i < numItems; i++)
        dequeue(x);
}

template <class T>
bool StaticQueue<T>::isEmpty()
{
    return !numItems;
}

template <class T>
bool StaticQueue<T>::isFull()
{
    return numItems == (size - 1);
}

#endif	/* STATICQUEUE_H */