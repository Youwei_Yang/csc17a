/* 
 * File:   main.cpp
 * Author: Stanley
 *
 * Created on December 2, 2016, 4:26 PM
 */

#include <cstdlib>
#include <iostream>
#include <string>
#include "StaticStack.h"
#include "DynamicStack.h"
#include "StaticQueue.h"
#include "DynamicQueue.h"
#include "StringStack.h"

using namespace std;


void line();
/*
 * 
 */
int main(int argc, char** argv) {

    bool run = true;
    while(run)
    {
        int menuInput;
        cout << "HW 9" << endl << endl;
        cout << "1) 18.1 Static Stack Template" << endl
                << "2) 18.2 Dynamic Stack Template" << endl
                << "3) 18.3 Static Queue Template" << endl
                << "4) 18.4 Dynamic Queue Template" << endl
                << "5) 18.5 Error Testing" << endl
                << "6) 18.6 Dynamic String Stack" << endl
                << "7) Exit" << endl
                << "Input: ";
        cin >> menuInput;
        
        line();
        
        if(menuInput <= 0 || menuInput > 7)
        {
            cout << "ERROR: INVALID INPUT" << endl;
        }
        
        switch(menuInput)
        {
            case 1:
            {
                cout << "18.1 Static Stack Template" << endl << endl;
                
                StaticStack<int> newStaticStack(3);
                int intStack;
                
                cout << "Trying to POP" << endl;
                newStaticStack.pop(intStack);
                
                cout << "PUSH 5" << endl;
                newStaticStack.push(5);
                cout << "Result: ";
                newStaticStack.print();
                
                cout << "PUSH 3" << endl;
                newStaticStack.push(3);
                cout << "Result: ";
                newStaticStack.print();
                
                cout << "PUSH 9" << endl;
                newStaticStack.push(9);
                cout << "Result: ";
                newStaticStack.print();
                
                cout << "Trying to PUSH 2" << endl;
                newStaticStack.push(2);
                
                cout << "POP the 9" << endl;
                newStaticStack.pop(intStack);
                cout << "Result: ";
                newStaticStack.print();
                
                cout << "POP the 3" << endl;
                newStaticStack.pop(intStack);
                cout << "Result: ";
                newStaticStack.print();
                
                break;
            }
            
            case 2:
            {
                cout << "18.2 Dynamic Stack Template" << endl << endl;
                
                DynamicStack<char> newDynamicStack;
                char charStack;
                
                cout << "PUSH \'Y\'" << endl;
                newDynamicStack.push('Y');
                cout << "Result: ";
                newDynamicStack.print();
                
                cout << "PUSH \'E\'" << endl;
                newDynamicStack.push('E');
                cout << "Result: ";
                newDynamicStack.print();
                
                cout << "PUSH \'H\'" << endl;
                newDynamicStack.push('H');
                cout << "Result: ";
                newDynamicStack.print();
                
                cout << "POP \'H\'" << endl;
                newDynamicStack.pop(charStack);
                cout << "Result: ";
                newDynamicStack.print();
                
                cout << "POP \'E\'" << endl;
                newDynamicStack.pop(charStack);
                cout << "Result: ";
                newDynamicStack.print();
                
                break;
            }
            
            case 3:
            {
                cout << "18.3 Static Queue Template" << endl << endl;
                
                StaticQueue<string> newStaticQueue(3);
                string stringQueue;
                
                cout << "ENQUEUE String \"Hello\"" << endl;
                newStaticQueue.enqueue("Hello");
                
                cout << "ENQUEUE String \"World\"" << endl;
                newStaticQueue.enqueue("World");
                
                cout << "DEQUEUE" << endl;
                newStaticQueue.dequeue(stringQueue);
                cout << "Result: " << stringQueue << " has been dequeued" << endl;
                
                cout << "CLEAR the queue and try to DEQUEUE" << endl;
                newStaticQueue.clear();
                newStaticQueue.dequeue(stringQueue);
                
                break;
            }
            
            case 4:
            {
                cout << "18.4 Dynamic Queue Template" << endl << endl;
                
                DynamicQueue<double> newDynamicQueue;
                double doubleQueue;
                
                cout << "ENQUEUE double: 1.2" << endl;
                newDynamicQueue.enqueue(1.2);
                
                cout << "ENQUEUE double: 6.32" << endl;
                newDynamicQueue.enqueue(6.32);
                
                cout << "DEQEUE" << endl;
                newDynamicQueue.dequeue(doubleQueue);
                cout << "Result: " << doubleQueue << " has been dequeued" << endl;
                
                cout << "CLEAR the queue and try to DEQUEUE" << endl;
                newDynamicQueue.clear();
                newDynamicQueue.dequeue(doubleQueue);
                
                break;
            }
            
            case 5:
            {
                cout << "18.5 Error Testing" << endl << endl;
                
                cout << "Implemented in 18.2 and 18.4 examples" << endl;
                break;
            }
            
            case 6:
            {
                cout << "18.6 Dynamic String Stack" << endl << endl;
                
                StringStack newStringStack;
                string stringStack;
                
                cout << "PUSH \"Hello\"" << endl;
                newStringStack.push("Hello");
                cout << "Result: ";
                newStringStack.print();
                
                cout << "PUSH \"World\"" << endl;
                newStringStack.push("World");
                cout << "Result: ";
                newStringStack.print();
                
                cout << "POP \"World\"" << endl;
                newStringStack.pop(stringStack);
                cout << "Result: ";
                newStringStack.print();
                
                break;
            }
            
            case 7:
            {
                return 0;
                break;
            }
        }
        line();
    }
    
    return 0;
}

void line()
{
    for (int i = 0; i < 30; i++)
    {
        cout << "-";
    }
    cout << endl;
}