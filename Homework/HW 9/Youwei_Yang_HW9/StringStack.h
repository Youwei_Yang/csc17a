/* 
 * File:   StringStack.h
 * Author: Stanley
 *
 * Created on December 5, 2016, 12:27 AM
 */

#ifndef STRINGSTACK_H
#define	STRINGSTACK_H
#include <cstdlib>
#include <iostream>
#include <string>

using namespace std;

class StringStack
{
public:
    // Constructor
    StringStack();
    virtual ~StringStack();
    
    // Other Functions
    void push(string x);
    void pop(string&x);
    void print();
private:
    struct StringNode
    {
        string val;
        StringNode* tail;
    };
    StringNode* top;
};

StringStack::StringStack()
{
    top = NULL;
}

StringStack::~StringStack()
{
    StringNode* temp = top;
    StringNode* tempTail;
    
    while(temp)
    {
        tempTail = temp->tail;
        delete temp;
        temp = tempTail;
    }
}

void StringStack::push(string x)
{
    try{
        StringNode* newNode = new StringNode();
        newNode->val = x;
        newNode->tail = top;
        top = newNode;
    }
    catch(bad_alloc)
    {
        cout << "ERROR: Cannot allocate memory" << endl;
        exit(EXIT_FAILURE);
    }
}

void StringStack::pop(string& x)
{
    if(!top)
        return;
    else
    {
        StringNode*temp = top->tail;
        x = top->val;
        delete top;
        top = temp;
    }
}

void StringStack::print()
{
    StringNode* temp = top;
    while(temp)
    {
        cout << temp->val << " ";
        temp = temp->tail;
    }
    
    cout << endl;
}

#endif	/* STRINGSTACK_H */

