/* 
 * File:   Node.h
 * Author: Stanley
 *
 * Created on December 4, 2016, 4:16 PM
 */

#ifndef NODE_H
#define	NODE_H
#include <cstdlib>
#include <iostream>

using namespace std;

template <class T>
class Node
{
public:
    // Constructor
    Node();
    
    // Public Variables
    T val;
    Node* tail;
private:

};

template <class T>
Node<T>::Node()
{
    tail = NULL;
}

#endif	/* NODE_H */

