/* 
 * File:   DynamicStack.h
 * Author: Stanley
 *
 * Created on December 4, 2016, 4:20 PM
 */

#ifndef DYNAMICSTACK_H
#define	DYNAMICSTACK_H
#include "NodeTemplate.h"

template <class T>
class DynamicStack
{
public:
    // Constructor
    DynamicStack();
    virtual ~DynamicStack<T>();
    
    // Other Functions
    void push(T x);
    void pop(T&x);
    void print();
private:
    Node<T>* top;
};

template<class T>
DynamicStack<T>::DynamicStack()
{
    top = NULL;
}

template<class T>
DynamicStack<T>::~DynamicStack()
{
    Node<T>* temp = top;
    Node<T>* tempTail;
    
    while(temp)
    {
        tempTail = temp->tail;
        delete temp;
        temp = tempTail;
    }
}

template<class T>
void DynamicStack<T>::push(T x)
{
    try{
        Node<T>* newNode = new Node<T>();
        newNode->val = x;
        newNode->tail = top;
        top = newNode;
    }
    catch(bad_alloc)
    {
        cout << "ERROR: Cannot allocate memory" << endl;
        exit(EXIT_FAILURE);
    }
}

template<class T>
void DynamicStack<T>::pop(T& x)
{
    if(!top)
        return;
    else
    {
        Node<T>*temp = top->tail;
        x = top->val;
        delete top;
        top = temp;
    }
}

template<class T>
void DynamicStack<T>::print()
{
    Node<T>* temp = top;
    while(temp)
    {
        cout << temp->val << " ";
        temp = temp->tail;
    }
    
    cout << endl;
}

#endif	/* DYNAMICSTACK_H */

