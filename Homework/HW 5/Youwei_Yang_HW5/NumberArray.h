/* 
 * File:   NumberArray.h
 * Author: Stanley
 *
 * Created on October 8, 2016, 5:16 PM
 */

#ifndef NUMBERARRAY_H
#define	NUMBERARRAY_H

#include <cstdlib>
#include <iostream>

using namespace std;

class NumberArray
{
public:
    // Getter & Setter
    void setSize(int);
    int getSize();
    
    // Constructor
    NumberArray(); // Default Constructor
    NumberArray(int, int[]);
    NumberArray(const NumberArray& orig); // Copy Constructor
    
    // Destructor
    ~NumberArray();
    
    // Operators
    /**
     * To store a number a set location
     * @param loc is the location of the array
     * @param number is the number to be stored
     */
    void storeNumber(int loc, int number);
    /**
     * Retrieve a number at a set location
     * @param loc is the location of the array to be retrieved
     * @return the number at the set location
     */
    int retrieveNumber(int loc);
    int highest();
    int lowest();
    int average();
    void printArray();
private:
    int*numberArray;
    int size;
};

#endif	/* NUMBERARRAY_H */

