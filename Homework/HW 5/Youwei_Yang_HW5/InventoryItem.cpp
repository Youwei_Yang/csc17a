/* 
 * File:   InventoryItem.cpp
 * Author: Stanley
 * 
 * Created on October 9, 2016, 5:16 PM
 */

#include "InventoryItem.h"

InventoryItem::InventoryItem()
{
    itemNumber = 0;
    quantity = 0;
    cost = 0;
}

InventoryItem::InventoryItem(int itemNumber, int quantity, double cost)
{
    this->itemNumber = itemNumber;
    this->quantity = quantity;
    this->cost = cost;
}

InventoryItem::InventoryItem(const InventoryItem& orig)
{
    this->itemNumber = orig.itemNumber;
    this->quantity = orig.quantity;
    this->cost = orig.cost;
}

InventoryItem::~InventoryItem()
{
    
}

void InventoryItem::setItemNumber(int itemNumber)
{
    this->itemNumber = itemNumber;
}

void InventoryItem::setQuantity(int quantity)
{
    this->quantity = quantity;
}

void InventoryItem::setCost(double cost)
{
    this->cost = cost;
}

void InventoryItem::setTotalCost()
{
    totalCost = quantity * cost;
}

int InventoryItem::getItemNumber()
{
    return itemNumber;
}

int InventoryItem::getQuantity()
{
    return quantity;
}

double InventoryItem::getCost()
{
    return cost;
}

double InventoryItem::getTotalCost()
{
    return totalCost;
}