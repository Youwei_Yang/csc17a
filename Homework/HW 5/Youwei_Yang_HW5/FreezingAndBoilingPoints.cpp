/* 
 * File:   FreezingAndBoilingPoints.cpp
 * Author: Stanley
 * 
 * Created on October 9, 2016, 1:02 AM
 */

#include "FreezingAndBoilingPoints.h"

FreezingAndBoilingPoints::FreezingAndBoilingPoints()
{
    this->temperature = 0;
}

FreezingAndBoilingPoints::FreezingAndBoilingPoints(int temperature)
{
    this->temperature = temperature;
}

FreezingAndBoilingPoints::FreezingAndBoilingPoints(const FreezingAndBoilingPoints& orig)
{
    this->temperature = orig.temperature;
}

FreezingAndBoilingPoints::~FreezingAndBoilingPoints()
{
    
}

void FreezingAndBoilingPoints::setTemperature(int temperature)
{
    this->temperature = temperature;
}

int FreezingAndBoilingPoints::getTemperature()
{
    return this->temperature;
}

bool FreezingAndBoilingPoints::isEthylFreezing()
{
    if(temperature <= -173) return true;
    else return false;
}

bool FreezingAndBoilingPoints::isEthylBoiling()
{
    if(temperature >= 172) return true;
    else return false;
}

bool FreezingAndBoilingPoints::isOxygenFreezing()
{
    if(temperature <= -362) return true;
    else return false;
}

bool FreezingAndBoilingPoints::isOxygenBoiling()
{
    if(temperature >= -306) return true;
    else return false;
}

bool FreezingAndBoilingPoints::isWaterFreezing()
{
    if(temperature <= 32) return true;
    else return false;
}

bool FreezingAndBoilingPoints::isWaterBoiling()
{
    if(temperature >= 212) return true;
    else return false;
}