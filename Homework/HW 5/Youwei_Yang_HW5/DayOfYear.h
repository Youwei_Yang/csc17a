/* 
 * File:   DayOfYear.h
 * Author: Stanley
 *
 * Created on October 9, 2016, 7:16 PM
 */

#ifndef DAYOFYEAR_H
#define	DAYOFYEAR_H

#include <cstdlib>
#include <iostream>

using namespace std;

class DayOfYear
{
public:
    // Getters & Setters
    void setDay(int);
    
    // Constructor
    DayOfYear(); // Default constructor
    DayOfYear(int);
    DayOfYear(const DayOfYear& orig); // Copy constructor
    
    // Destructor
    ~DayOfYear();
    
    // Operator
    string printMonth(int);
    int getMonth();
    void print();
    
private:
    int day;
    int dayLeft;
    int month;
};

#endif	/* DAYOFYEAR_H */

