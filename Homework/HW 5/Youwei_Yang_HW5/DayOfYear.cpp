/* 
 * File:   DayOfYear.cpp
 * Author: Stanley
 * 
 * Created on October 9, 2016, 7:16 PM
 */

#include "DayOfYear.h"

DayOfYear::DayOfYear()
{
    day = 0;
}

DayOfYear::DayOfYear(int day)
{
    this->day = day;
}

DayOfYear::DayOfYear(const DayOfYear& orig)
{
    this->day = orig.day;
}

DayOfYear::~DayOfYear()
{
    
}

void DayOfYear::setDay(int day)
{
    this->day = day;
}

string DayOfYear::printMonth(int month)
{
    string months[] = {"January", "February", "March", "April", "May", "June", "July",
                        "August", "September", "October", "November", "December"};
    
    return months[month];
}

int DayOfYear::getMonth()
{
    int monthList[] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    dayLeft = day;
    
    for(int i = 0; i < 12; i++)
    {
        if((dayLeft - monthList[i]) <= 0) return month;
        else
        {
            dayLeft  = dayLeft - monthList[i];
            month++;
        }
    }
    return month;
}

void DayOfYear::print()
{
    cout << "Day " << day << " would be " << printMonth(getMonth()) << " " << dayLeft << endl;
}