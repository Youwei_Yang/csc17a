/* 
 * File:   Coin.h
 * Author: Stanley
 *
 * Created on October 8, 2016, 11:08 PM
 */

#ifndef COIN_H
#define	COIN_H

#include <cstdlib>
#include <iostream>

using namespace std;

class Coin
{
public:
    // Getters & Setters
    string getSideUp();
    
    // constructor
    Coin(); // Default
    Coin(const Coin& orig); // Copy constructor
    
    //Destructor
    ~Coin();
    
    // Operators
    void toss();
    
private:
    string sideUp;
};

#endif	/* COIN_H */

