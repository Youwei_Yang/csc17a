/* 
 * File:   Coin.cpp
 * Author: Stanley
 * 
 * Created on October 8, 2016, 11:08 PM
 */

#include "Coin.h"

Coin::Coin()
{
    toss();
}

Coin::Coin(const Coin& orig)
{
    this->sideUp = orig.sideUp;
}

Coin::~Coin()
{
    
}

string Coin::getSideUp()
{
    return sideUp;
}

void Coin::toss()
{
    if((rand() % 2) == 0)
    {
        this->sideUp = "heads";
    } else
    {
        this->sideUp = "tails";
    }
}