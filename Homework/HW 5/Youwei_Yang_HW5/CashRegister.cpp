/* 
 * File:   CashRegister.cpp
 * Author: Stanley
 * 
 * Created on October 9, 2016, 5:23 PM
 */

#include "CashRegister.h"

CashRegister::CashRegister()
{
    this->profit = 0.00;
    this->tax = 0.00;
}

CashRegister::CashRegister(double profit, double tax)
{
    this->profit = profit;
    this->tax = tax;
}

CashRegister::CashRegister(const CashRegister& orig)
{
    this->profit = orig.profit;
    this->tax = orig.tax;
}

CashRegister::~CashRegister()
{
    
}

void CashRegister::setProfit(double profit)
{
    this->profit = profit;
}

double CashRegister::getProfit()
{
    return profit;
}

void CashRegister::setTax(double tax)
{
    this->tax = tax;
}

double CashRegister::getTax()
{
    return tax;
}

void CashRegister::setCost(double cost)
{
    this->cost = cost;
}

double CashRegister::getCost()
{
    return cost;
}

void CashRegister::setQuantity(int quantity)
{
    this->quantity = quantity;
}

int CashRegister::getQuantity()
{
    return quantity;
}

double CashRegister::getSubTotal()
{
    double unitPrice = cost + (cost * profit);
    this->subTotal = unitPrice * quantity;
    
    return subTotal;
}

double CashRegister::getTotal()
{
    this->total = subTotal + (subTotal * tax);

    return total;
}