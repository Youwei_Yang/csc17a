/* 
 * File:   InventoryItem.h
 * Author: Stanley
 *
 * Created on October 9, 2016, 5:16 PM
 */

#ifndef INVENTORYITEM_H
#define	INVENTORYITEM_H

class InventoryItem {
public:
    // Setters & Getters
    void setItemNumber(int);
    void setQuantity(int);
    void setCost(double);
    void setTotalCost();
    int getItemNumber();
    int getQuantity();
    double getCost();
    double getTotalCost();
    
    // Constructor
    InventoryItem(); // Default constructor
    InventoryItem(int, int, double);
    InventoryItem(const InventoryItem& orig); // Copy constructor 
    
    // Destructor
    ~InventoryItem();
private:
    int itemNumber;
    int quantity;
    double cost;
    double totalCost;
};

#endif	/* INVENTORYITEM_H */

