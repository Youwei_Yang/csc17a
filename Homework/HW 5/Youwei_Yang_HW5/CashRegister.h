/* 
 * File:   CashRegister.h
 * Author: Stanley
 *
 * Created on October 9, 2016, 5:23 PM
 */

#ifndef CASHREGISTER_H
#define	CASHREGISTER_H

class CashRegister
{
public:
    // Getters && Setters
    void setProfit(double);
    double getProfit();
    void setTax(double);
    double getTax();
    void setCost(double);
    double getCost();
    void setQuantity(int);
    int getQuantity();
    
    // Constructor
    CashRegister(); // Default constructor
    /**
     * Create a cash register with selected profit and tax percentage
     * @param profit is the percent of profit to make
     * @param tax is the percent of tax to add to the item
     */
    CashRegister(double profit, double tax);
    CashRegister(const CashRegister& orig); // Copy constructor
    
    // Destructor
    ~CashRegister();
    
    // Operators
    double getSubTotal();
    double getTotal();
    
private:
    double profit;
    double tax;
    double cost;
    int quantity;
    double subTotal;
    double total;
};

#endif	/* CASHREGISTER_H */

