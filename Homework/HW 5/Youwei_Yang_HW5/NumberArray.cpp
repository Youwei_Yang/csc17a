/* 
 * File:   NumberArray.cpp
 * Author: Stanley
 * 
 * Created on October 8, 2016, 5:16 PM
 */

#include "NumberArray.h"

NumberArray::NumberArray()
{
    this->size = 0;
    this->numberArray = NULL;
}

NumberArray::NumberArray(int size, int numberArray[])
{
    this->size = size;
    this->numberArray = new int[size];
    for(int i = 0; i < size; i++)
    {
        this->numberArray[i] = numberArray[i];
    }
}

NumberArray::NumberArray(const NumberArray& orig)
{
    this->size = orig.size;
    this->numberArray = new int[size];
    for(int i = 0; i < size; i++)
    {
        this->numberArray[i] = orig.numberArray[i];
    }
}

NumberArray::~NumberArray()
{
    delete[] numberArray;
}

void NumberArray::setSize(int size)
{
    this->size = size;
    this->numberArray = new int [size];
}

int NumberArray::getSize()
{
    return this->size;
}

void NumberArray::storeNumber(int loc, int number)
{
    this->numberArray[loc] = number;
}

int NumberArray::retrieveNumber(int loc)
{
    return this->numberArray[loc];
}

int NumberArray::highest()
{
    int highest = 0;
    
    for(int i = 0; i < size; i++)
    {
        if(numberArray[i] > highest)
        {
            highest = numberArray[i];
        }
    }
    
    return highest;
}

int NumberArray::lowest()
{
    int lowest = 0;
    
    for(int i = 0; i < size; i++)
    {
        if(numberArray[i] < lowest)
        {
            lowest = numberArray[i];
        }
    }
    
    return lowest;
}

int NumberArray::average()
{
    int average = 0;
    
    for(int i = 0; i < size; i++)
    {
        average = average + numberArray[i];
    }
    
    return (average/size);
}

void NumberArray::printArray()
{
    for(int i = 0; i < size; i++)
    {
        cout << numberArray[i] << " ";
    }
    cout << endl;
}