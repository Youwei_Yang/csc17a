/* 
 * File:   main.cpp
 * Author: Stanley
 *
 * Created on October 8, 2016, 3:23 PM
 */

#include <cstdlib>
#include <iostream>

#include "NumberArray.h"
#include "Coin.h"
#include "FreezingAndBoilingPoints.h"
#include "InventoryItem.h"
#include "CashRegister.h"
#include "DayOfYear.h"

using namespace std;

void line();
void randNumberArray(int, int[]);

/*
 * 
 */
int main(int argc, char** argv) {

    srand(time(NULL));
    bool run = true;
    
    while(run)
    {
        // Menu
        int menuInput;
        cout << "HW 5" << endl 
                << "1) 13.10 Number array class" << endl 
                << "2) 13.12 Coin toss simulator" << endl 
                << "3) 13.16 Freezing and Boiling points" << endl 
                << "4) 13.17 Cash Register" << endl 
                << "5) 14.2 Day of the Year" << endl
                << "6) Exit" << endl
                << "Input: ";
        cin >> menuInput;
        
        line();
        
        switch(menuInput)
        {
            case 1:
            {
                cout << "13.10 Number array class" << endl << endl;
                
                int size = 10;
                int numberArray[size];
                randNumberArray(size, numberArray);
                NumberArray newNumberArray(size, numberArray);
                
                cout << "Original array: ";
                newNumberArray.printArray();
                cout << "Change 5th element to 77: ";
                newNumberArray.storeNumber(4, 77);
                newNumberArray.printArray();
                cout << "3rd element: ";
                newNumberArray.retrieveNumber(2);
                cout << "Highest value: " << newNumberArray.highest() << endl;
                cout << "Lowest value: " << newNumberArray.lowest() << endl;
                cout << "Average: " << newNumberArray.average() << endl;
                
                break;
            }
            
            case 2:
            {
                cout << "Coin toss simulator" << endl << endl;
                
                Coin newCoin;
                int headsUp = 0;
                int tailsUp = 0;
                for(int i = 0; i < 10; i++)
                {
                    newCoin.toss();
                    if(newCoin.getSideUp() == "heads")
                    {
                        headsUp++;
                    } else
                    {
                        tailsUp++;
                    }
                    cout << newCoin.getSideUp() << " ";
                }
                cout << endl;
                cout << "Total heads: " << headsUp << endl;
                cout << "Total tails: " << tailsUp << endl;
                
                break;
            }
            
            case 3:
            {
                cout << "Freezing and Boiling points" << endl << endl;
                
                int temperature;
                FreezingAndBoilingPoints newTemperature;
                cout << "Enter a temperature: ";
                cin >> temperature;
                newTemperature.setTemperature(temperature);
                if(newTemperature.isEthylFreezing())
                {
                    cout << "Ethyl will freeze" << endl;
                } else if(newTemperature.isEthylBoiling())
                {
                    cout << "Ethyl will boil" << endl;
                }
                
                if(newTemperature.isOxygenFreezing())
                {
                    cout << "Oxygen will freeze" << endl;
                } else if(newTemperature.isOxygenBoiling())
                {
                    cout << "Oxygen will boil" << endl;
                }
                
                if(newTemperature.isWaterFreezing())
                {
                    cout << "Water will freeze" << endl;
                } else if(newTemperature.isWaterBoiling())
                {
                    cout << "Water will boil" << endl;
                }
                
                break;
            }
            
            case 4:
            {
                cout << "Cash Register" << endl << endl;
                
                const int inventorySize = 3;
                int itemSelection;
                int quantitySelection;
                
                // Creating items
                InventoryItem items[inventorySize];
                items[0].setItemNumber(159753); // item 1
                items[0].setCost(9.99);
                items[0].setQuantity(5);
                items[1].setItemNumber(456852); // item 2
                items[1].setCost(5.59);
                items[1].setQuantity(21);
                items[2].setItemNumber(123987); // item 3
                items[2].setCost(14.99);
                items[2].setQuantity(6);
                
                // Print inventory items
                for(int i = 0; i < inventorySize; i++)
                {
                    cout << (i + 1) << ") "
                            << "#: " << items[i].getItemNumber() << endl
                            << "Cost: " << items[i].getCost() << endl
                            << "Quantity: " << items[i].getQuantity() << endl;
                }
                cout << endl;
                
                cout << "Use number 1 ~" << inventorySize << " to select one of above item" << endl;
                cout << "Selection: ";
                cin >> itemSelection;
                if(itemSelection <= 0 || itemSelection > inventorySize){
                    cout << "ERROR: OUT OF RANGE" << endl;
                    break;
                }
                cout << "Quantity to be purchased: ";
                cin >> quantitySelection;
                if(quantitySelection <= 0 || quantitySelection > items[itemSelection - 1].getQuantity()){
                    cout << "ERROR: OUT OF RANGE" << endl;
                    break;
                }
                cout << endl;
                
                // Modify quantity
                items[itemSelection - 1].setQuantity(items[itemSelection - 1].getQuantity() - quantitySelection);
                
                // Creating new cash register
                CashRegister newCashRegister(0.30, 0.06);
                newCashRegister.setQuantity(quantitySelection);
                newCashRegister.setCost(items[itemSelection - 1].getCost());
                
                printf("PURCHASE TOTAL: $%.2f\n", newCashRegister.getSubTotal());
                cout << "TAX: " << newCashRegister.getTax() << "%" << endl;
                printf("TOTAL: $%.2f\n", newCashRegister.getTotal());
                cout << endl;
                
                break;
            }
            
            case 5:
            {
                cout << "Day of the Year" << endl << endl;
                
                int dayInput;
                
                cout << "Input a day of the year: ";
                cin >> dayInput;
                
                DayOfYear newDay(dayInput);
                newDay.print();
                
                break;
            }
            
            case 6:
            {
                return 0;
            }
        }
        
        line();
    }
    
    return 0;
}

void line()
{
    for(int i = 0; i < 35; i++)
    {
        cout << "-";
    }
    cout << endl;
}

void randNumberArray(int size, int array[])
{
    for(int i = 0; i < size; i++)
    {
        array[i] = rand() % 100;
    }
}