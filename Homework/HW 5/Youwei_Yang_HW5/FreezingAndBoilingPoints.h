/* 
 * File:   FreezingAndBoilingPoints.h
 * Author: Stanley
 *
 * Created on October 9, 2016, 1:02 AM
 */

#ifndef FREEZINGANDBOILINGPOINTS_H
#define	FREEZINGANDBOILINGPOINTS_H

#include <iostream>
#include <cstdlib>

using namespace std;

class FreezingAndBoilingPoints
{
public:
    // Getters & Setter
    void setTemperature(int);
    int getTemperature();
    
    // Constructor
    FreezingAndBoilingPoints(); // Default constructor
    FreezingAndBoilingPoints(int);
    FreezingAndBoilingPoints(const FreezingAndBoilingPoints& orig); // Copy constructor
    
    // Destructor
    ~FreezingAndBoilingPoints();
    
    //Operators
    bool isEthylFreezing();
    bool isEthylBoiling();
    bool isOxygenFreezing();
    bool isOxygenBoiling();
    bool isWaterFreezing();
    bool isWaterBoiling();
    
private:
    int temperature;
};

#endif	/* FREEZINGANDBOILINGPOINTS_H */

