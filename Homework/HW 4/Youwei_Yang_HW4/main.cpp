/* 
 * File:   main.cpp
 * Author: Stanley
 *
 * Created on September 23, 2016, 11:04 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

// General
void line();

// 11.6 Soccer scores: Only use 3 players
struct SoccerPlayer
{
    string name;
    int number;
    int points;
};
void inputPlayer(SoccerPlayer*&, int);
void outputPlayer(SoccerPlayer*&, int);

// 11.5 Weather statistics modification
struct WeatherStatistics
{
    double totalRainfall;
    double highTemp;
    double lowTemp;
    double averageTemp;
};
enum month {JANUARY, FEBRUARY, MARCH, APRIL, MAY, JUNE, JULY, AUGUST, SEPTEMBER, OCTOBER, NOVEMBER, DECEMBER};
void inputWeather(WeatherStatistics*&, int);
void outputWeather(WeatherStatistics*&, int);

// 13.1 Date
class Date
{
private:
    int month;
    int day;
    int year;
public:
    string toMonth();
    void printDate1();
    void printDate2();
    void printDate3();
    // Constructor
    Date();
    Date(int, int, int);
};

// 13.2 Employee class
class Employee
{
private:
    string name;
    int idNumber;
    string department;
    string position;
public:
    // Setter
    void setName(string);
    void setIDNumber(int);
    void setDepartment(string);
    void setPosition(string);
    // Getter
    string getName();
    int getIDNumber();
    string getDepartment();
    string getPosition();
    // Constructor
    Employee();
    Employee(string, int);
    Employee(string, int, string, string);
};
void printEmployee(Employee);

//13.3 Car class
class Car
{
private:
    int yearModel;
    string make;
    int speed;
public:
    void accelerate();
    void brake();
    // Getter
    int getYearModel();
    string getMake();
    int getSpeed();
    //Constructor
    Car(int, string);
};

//13.6 Inventory Class
class Inventory
{
private:
    int itemNumber;
    int quantity;
    double cost;
    double totalCost;
public:
    // Setter
    void setItemNumber(int);
    void setQuantity(int);
    void setCost(double);
    void setTotalCost();
    // Getter
    int getItemNumber();
    int getQuantity();
    double getCost();
    double getTotalCost();
    // Constructor
    Inventory();
    Inventory(int, int, double);
};
bool isNegative(double);

/*
 * 
 */
int main(int argc, char** argv) {

    bool run = true;
    
    while (run)
    {
        
        // Menu
        cout << "HW4" << endl << endl;
        cout << "1) 11.6 Soccer scores: Only use 3 players" << endl;
        cout << "2) 11.5 Weather statistics modification" << endl;
        cout << "3) 13.1 Date" << endl;
        cout << "4) 13.2 Employee class" << endl;
        cout << "5) 13.3 Car class" << endl;
        cout << "6) 13.6 Inventory Class" << endl;
        cout << "Input: ";
        
        int menuInput;
        cin >> menuInput;
        line();
        
        switch(menuInput)
        {
            case 1: // 11.6 Soccer scores: Only use 3 players
            {
                cout << "11.6 Soccer scores: Only use 3 players" << endl << endl;
                
                int size = 3;
                SoccerPlayer*players = new SoccerPlayer[size];
                inputPlayer(players, size);
                outputPlayer(players, size);
                break;
            }
            
            case 2: // 11.5 Weather statistics modification
            {
                cout << "11.5 Weather statistics modification" << endl << endl;
                
                int size = 12;
                WeatherStatistics*weather = new WeatherStatistics[size];
                inputWeather(weather, size);
                outputWeather(weather, size);
                break;
            }
            
            case 3: // 13.1 Date
            {
                cout << "13.1 Date" << endl << endl;
                
                Date date;
                
                date.printDate1();
                date.printDate2();
                date.printDate3();
                break;
            }
            
            case 4: //13.2 Employee class
            {
                cout << "13.2 Employee class" << endl << endl;
                
                Employee susanMeyers("Susan Meyers", 47899, "Accounting", "Vice President");
                Employee markJones("Mark Jones", 39119, "IT", "Programmer");
                Employee joyRogers("Joy Rogers", 81774, "Manufacturing", "Engineer");
                
                printEmployee(susanMeyers);
                printEmployee(markJones);
                printEmployee(joyRogers);
                break;
            }
            
            case 5: // 13.3 Car class
            {
                cout << "13.3 Car class" << endl << endl;
                
                Car hondaCivic(2016, "Honda");
                
                cout << hondaCivic.getMake() << ", " << hondaCivic.getYearModel() << endl << endl;
                
                // Accelerate
                cout << "Accelerate" << endl;
                for(int i = 0; i < 5; i++)
                {
                    cout << "Current speed: " << hondaCivic.getSpeed() << endl;
                    hondaCivic.accelerate();
                }
                cout << endl;
                
                // Brake
                cout << "Brake" << endl;
                for(int i = 0; i < 3; i++)
                {
                    cout << "Current speed: " << hondaCivic.getSpeed() << endl;
                    hondaCivic.brake();
                }
                break;
            }
            
            case 6: // 13.6 Inventory Class
            {
                cout << "13.6 Inventory Class" << endl << endl;
                
                int itemNumber;
                int quantity;
                double cost;
                
                cout << "Input item number: ";
                cin >> itemNumber;
                if(isNegative(itemNumber)){
                    cout << "ERROR: NEGATIVE VALUES" << endl;
                    break;
                }
                cout << "Input quantity: ";
                cin >> quantity;
                if(isNegative(quantity)){
                    cout << "ERROR: NEGATIVE VALUES" << endl;
                    break;
                }
                cout << "Input cost: ";
                cin >> cost;
                if(isNegative(cost)){
                    cout << "ERROR: NEGATIVE VALUES" << endl;
                    break;
                }
                
                Inventory newItem(itemNumber, quantity, cost);
                newItem.setTotalCost();
                
                cout << "Total cost: $" << newItem.getTotalCost() << endl;
            }
        }
        
        line();
        
    }
    
    return 0;
}

// General ---------------------------------------------------------------------
void line()
{
    for(int i = 0; i < 45; i++){
        cout << "-";
    }
    cout << endl;
}

// 11.6 Soccer scores: Only use 3 players --------------------------------------
void inputPlayer(SoccerPlayer*&players, int size)
{
    string name;
    int number;
    
    for(int i = 0; i < size; i++)
    {
        cout << "Please enter the name for player" << (i+1) << ": ";
        cin >> name;
        players[i].name = name;
        cout << "Please enter " << players[i].name << "'s number: ";
        cin >> number;
        players[i].number = number;
        cout << "Please enter " << players[i].name << "'s points: ";
        cin >> number;
        players[i].points = number;
        cout << endl;
    }
}

void outputPlayer(SoccerPlayer*&players, int size)
{
    int totalPoints = 0;
    int mostPoint = 0;
    SoccerPlayer topPlayer;
    
    // Printout player info
    for(int i = 0; i < size; i++)
    {
        cout << "PLAYER " << (i+1) << endl;
        cout << players[i].number << ", " << players[i].name << ": " << players[i].points << endl;
        cout << endl;
    }
    
    // Calculate total points earned by the team
    // Calculate the most points earned by a player
    for(int i = 0; i < size; i++)
    {
        totalPoints += players[i].points;
        
        if(players[i].points > mostPoint)
        {
            mostPoint = players[i].points;
            topPlayer = players[i];
        }
    }
    
    // Output calculation
    cout << "TOTAL POINTS: " << totalPoints << endl;
    cout << "BEST PLAYER: " << topPlayer.number << ", " << topPlayer.name << endl;
}

// 11.5 Weather statistics modification ----------------------------------------
void inputWeather(WeatherStatistics*&weather, int size)
{
    for(int i = JANUARY; i < DECEMBER; i++)
    {
        cout << "MONTH " << i + 1 << endl;
        cout << "Total Rainfall: ";
        cin >> weather[i].totalRainfall;
        cout << "High Temperature: ";
        cin >> weather[i].highTemp;
        cout << "Low Temperature: ";
        cin >> weather[i].lowTemp;
        cout << "Average Temperature: ";
        cin >> weather[i].averageTemp;
        cout << endl;
    }
}

void outputWeather(WeatherStatistics*&weather, int size)
{
    //calculating data
    double totalRainfall;
    double highestTemp = weather[0].highTemp;
    double lowestTemp = weather[0].lowTemp;
    double totalTemp;

    for(int i = 0; i < 12; i++){
        totalRainfall += weather[i].totalRainfall;
        totalTemp += weather[i].averageTemp;
        if(weather[i].highTemp > highestTemp)
        {
            highestTemp = weather[i].highTemp;
        }
        if(weather[i].lowTemp < lowestTemp)
        {
            lowestTemp = weather[i].lowTemp;
        }
    }

    cout << endl;

    //output data
    cout << "Total rainfall: " << totalRainfall << endl;
    cout << "Average monthly rainfall: " << totalRainfall/12 << endl;
    cout << "Highest temperature: " << highestTemp << endl;
    cout << "Lowest temperature: " << lowestTemp << endl;
    cout << "Average monthly temperature: " << totalTemp/12 << endl;
}

// 13.1 Date -------------------------------------------------------------------
string Date::toMonth()
{
    string months[] = {"January", "February", "March", "April", "May", "June", "July",
                        "August", "September", "October", "November", "December"};
    
    return months[month - 1];
}

void Date::printDate1()
{
    cout << month << "/" << day << "/" << year << endl;
}

void Date::printDate2()
{
    cout << toMonth() << " " << day << ", " << year << endl;
}

void Date::printDate3()
{
    cout << day << " " << toMonth() << " " << year << endl;
}

Date::Date()
{
    month = 12;
    day = 25;
    year = 2014;
}

Date::Date(int month, int day, int year)
{
    this->month = month;
    this->day = day;
    this->year = year;
}

//13.2 Employee class ----------------------------------------------------------
void Employee::setName(string name)
{
    this->name = name;
}

void Employee::setIDNumber(int idNumber)
{
    this->idNumber = idNumber;
}

void Employee::setDepartment(string department)
{
    this->department = department;
}

void Employee::setPosition(string position)
{
    this->position = position;
}

string Employee::getName()
{
    return name;
}

int Employee::getIDNumber()
{
    return idNumber;
}

string Employee::getDepartment()
{
    return department;
}

string Employee::getPosition()
{
    return position;
}

Employee::Employee()
{
    name = "";
    idNumber = 0;
    department = "";
    position = "";
}

Employee::Employee(string name, int idNumber)
{
    this->name = name;
    this->idNumber = idNumber;
    department = "";
    position = "";
}

Employee::Employee(string name, int idNumber, string department, string position)
{
    this->name = name;
    this->idNumber = idNumber;
    this->department = department;
    this->position = position;
}

void printEmployee(Employee employee)
{
    cout << "Name: " << employee.getName() << endl;
    cout << "ID Number: " << employee.getIDNumber() << endl;
    cout << "Department: " << employee.getDepartment() << endl;
    cout << "Position: " << employee.getPosition() << endl;
    cout << endl;
}

// 13.3 Car class --------------------------------------------------------------
void Car::accelerate()
{
    speed = speed + 5;
}

void Car::brake()
{
    speed = speed - 5;
}

int Car::getYearModel()
{
    return yearModel;
}

string Car::getMake()
{
    return make;
}

int Car::getSpeed()
{
    return speed;
}

Car::Car(int yearModel, string make)
{
    this->yearModel = yearModel;
    this->make = make;
    speed = 0;
}

// 13.6 Inventory Class --------------------------------------------------------
void Inventory::setItemNumber(int itemNumber)
{
    this->itemNumber = itemNumber;
}

void Inventory::setQuantity(int quantity)
{
    this->quantity = quantity;
}

void Inventory::setCost(double cost)
{
    this->cost = cost;
}

void Inventory::setTotalCost()
{
    totalCost = quantity * cost;
}

int Inventory::getItemNumber()
{
    return itemNumber;
}

int Inventory::getQuantity()
{
    return quantity;
}

double Inventory::getCost()
{
    return cost;
}

double Inventory::getTotalCost()
{
    return totalCost;
}

Inventory::Inventory()
{
    itemNumber = 0;
    quantity = 0;
    cost = 0;
}

Inventory::Inventory(int itemNumber, int quantity, double cost)
{
    this->itemNumber = itemNumber;
    this->quantity = quantity;
    this->cost = cost;
}

bool isNegative(double input)
{
    if(input < 0)
    {
        return true;
    }else{
        return false;
    }
}