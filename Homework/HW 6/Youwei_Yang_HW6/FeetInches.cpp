/* 
 * File:   FeetInches.cpp
 * Author: Stanley
 * 
 * Created on October 16, 2016, 1:05 AM
 */

#include "FeetInches.h"
#include <cstdlib>

FeetInches::FeetInches(int feet, int inches)
{
    this->feet = feet;
    this->inches = inches;
    simplify();
}

FeetInches::FeetInches(const FeetInches& orig)
{
    this->feet = orig.feet;
    this->inches = orig.inches;
}

FeetInches::~FeetInches()
{
    
}

void FeetInches::setFeet(int feet)
{
    this->feet = feet;
}

void FeetInches::setInches(int inches)
{
    this->inches = inches;
}

int FeetInches::getFeet()
{
    return feet;
}

int FeetInches::getInches()
{
    return inches;
}

void FeetInches::simplify()
{
    if(inches >= 12)
    {
        feet += (inches / 12);
        inches = inches % 12;
    }
    else if(inches < 0)
    {
        feet -= ((abs(inches) / 12) + 1);
        inches = 12 - (abs(inches) % 12);
    }
}

FeetInches FeetInches::operator+(const FeetInches&right)
{
    FeetInches temp;
    
    temp.inches = inches + right.inches;
    temp.feet = feet + right.feet;
    temp.simplify();
    return temp;
}

FeetInches FeetInches::operator-(const FeetInches&right)
{
    FeetInches temp;
    
    temp.inches = inches - right.inches;
    temp.feet = feet - right.feet;
    temp.simplify();
    return temp;
}

FeetInches FeetInches::operator++()
{
    ++inches;
    simplify();
    return *this;
}

FeetInches FeetInches::operator++(int)
{
    FeetInches temp(feet, inches);
    inches++;
    simplify();
    return temp;
}

bool FeetInches::operator>(const FeetInches&right)
{
    bool status;
    
    if(feet > right.feet)
        status = true;
    else if(feet == right.feet && inches > right.inches)
        status = true;
    else
        status = false;
    
    return status;
}

bool FeetInches::operator<(const FeetInches&right)
{
    bool status;
    
    if(feet < right.feet)
        status = true;
    else if(feet == right.feet && inches < right.inches)
        status = true;
    else
        status = false;
    
    return status;
}

bool FeetInches::operator==(const FeetInches&right)
{
    bool status;
    
    if(feet == right.feet && inches == right.inches)
        status = true;
    else
        status = false;
    
    return status;
}

bool FeetInches::operator<=(const FeetInches&right)
{
    bool status;
    
    if(feet < right.feet)
        status = true;
    else if(feet == right.feet && inches <= right.inches)
        status = true;
    else
        status = false;
    
    return status;
}

bool FeetInches::operator>=(const FeetInches&right)
{
    bool status;
    
    if(feet > right.feet)
        status = true;
    else if(feet == right.feet && inches >= right.inches)
        status = true;
    else
        status = false;
    
    return status;
}

bool FeetInches::operator!=(const FeetInches&right)
{
    bool status;
    
    if(feet != right.feet || inches != right.inches)
        status = true;
    else
        status = false;
    
    return status;
}

FeetInches FeetInches::mutiply(FeetInches&right)
{
    this->feet = this->feet * right.feet;
    this->inches = this->inches * right.inches;
    simplify();
    return *this;
}