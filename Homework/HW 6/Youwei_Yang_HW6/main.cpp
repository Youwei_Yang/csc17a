/* 
 * File:   main.cpp
 * Author: Stanley
 *
 * Created on October 15, 2016, 4:29 PM
 */

#include <cstdlib>
#include <iostream>
#include <fstream>

#include "DayOfYear.h"
#include "FeetInches.h"

using namespace std;

void line();
void randNum(int [], int);
void arrayToFile(string, int*&, int);
void fileToArray(string, int*&, int);

/*
 * 
 */
int main(int argc, char** argv) {

    bool run = true;
    srand(time(NULL));
    
    while(run)
    {
        cout << "HW 6" << endl << endl;
        cout << "1) 14.3 Day of the Year Modification" << endl
                << "2) 14.9 FeetInches Modification" << endl
                << "3) 14.11 FeetInches Class Copy Constructor and multiply function" << endl
                << "4) 12.9 File Encryption Filter" << endl
                << "5) 12.10 File Decryption Filter" << endl
                << "6) 12.8 Array/File Functions" << endl
                << "7) Exit" << endl;
        cout << "Please choice one of the above options: ";
        int menuInput;
        cin >> menuInput;
        
        line();
        
        switch(menuInput)
        {
            case 1:
            {
                string month = "";
                int day = 0;
                
                cout << "14.3 Day of the Year Modification" << endl << endl;
                
                cout << "Month (ex: January, February...): ";
                cin >> month;
                cout << "Day: ";
                cin >> day;
                
                DayOfYear newDay(month, day);
                newDay.print();
                
                cout << "Increment the day by 1:" << endl;
                ++newDay;
                newDay.print();
                cout << "Decrement the day by 1" << endl;
                newDay--;
                newDay.print();
                
                break;
            }
            
            case 2:
            {
                cout << "14.9 FeetInches Modification" << endl << endl;
                
                int feet;
                int inches;
                
                cout << "First measurement" << endl;
                cout << "Feet: ";
                cin >> feet;
                cout << "Inches: ";
                cin >> inches;
                FeetInches firstMeasurement(feet, inches);
                
                cout << endl << "Second measurement" << endl;
                cout << "Feet: ";
                cin >> feet;
                cout << "Inches: ";
                cin >> inches;
                FeetInches secondMeasurement(feet, inches);
                
                cout << endl;
                if(firstMeasurement <= secondMeasurement)
                    cout << "first measurement is <= second measurement" << endl;
                
                if(firstMeasurement >= secondMeasurement)
                    cout << "first measurement is >= second measurement" << endl;
                
                if(firstMeasurement != secondMeasurement)
                    cout << "first measurement is != second measurement" << endl;
                
                break;
            }
            
            case 3:
            {
                cout << "3) 14.11 FeetInches Class Copy Constructor and multiply function" << endl << endl;
                
                int feet;
                int inches;
                
                cout << "First measurement" << endl;
                cout << "Feet: ";
                cin >> feet;
                cout << "Inches: ";
                cin >> inches;
                FeetInches firstMeasurement(feet, inches);
                
                cout << endl << "Second measurement" << endl;
                cout << "Feet: ";
                cin >> feet;
                cout << "Inches: ";
                cin >> inches;
                FeetInches secondMeasurement(feet, inches);
                
                FeetInches thirdMeasurement(firstMeasurement.mutiply(secondMeasurement));
                
                cout << endl;
                cout << "first measurement multiply second measurement" << endl;
                cout << "Feet: " << thirdMeasurement.getFeet() << endl;
                cout << "Inches: " << thirdMeasurement.getInches() << endl;
                
                break;
            }
            
            case 4:
            {
                cout << "4) 12.9 File Encryption Filter" << endl << endl;
                
                fstream file;
                fstream file2;
                file.open("test.txt", ios::in);
                file2.open("encryptedTest.txt", ios::out);
                    
                while(!file.eof())
                {
                    char c = file.get() + 10;
                    file2.put(c);
                }
                
                file.close();
                file2.close();
                
                cout << "file encrypted" << endl;
                
                break;
            }
            
            case 5:
            {
                cout << "5) 12.10 File Decryption Filter" << endl << endl;
                
                fstream file;
                fstream file2;
                file.open("encryptedTest.txt", ios::in);
                file2.open("decryptedTest.txt", ios::out);
                
                while(!file.eof())
                {
                    char c = file.get() - 10;
                    file2.put(c);
                }
                
                file.close();
                file2.close();
                
                cout << "file decrypted" << endl;
                
                break;
            }
            
            case 6:
            {
                cout << "6) 12.8 Array/File Functions" << endl << endl;
                
                string fileName = "arrayTest.dat";
                int size = 10;
                int*intArray = new int[size];
                randNum(intArray, size);
                
                cout << "Content of the int array: ";
                for(int i = 0; i < size; i++)
                {
                    cout << intArray[i] << " ";
                }
                cout << endl;
                
                arrayToFile(fileName, intArray, size);
                
                cout << endl;
                
                int*newArray = new int[size];
                fileToArray(fileName, newArray, size);
                
                cout << "Content of the new int array: ";
                for(int i = 0; i < size; i++)
                {
                    cout << newArray[i] << " ";
                }
                
                cout << endl;
                
                break;
            }
            
            case 7:
            {
                return 0;
            }
        }
        
        line();

    }
    
    return 0;
}

void line()
{
    for (int i = 0; i < 45; i ++)
    {
        cout << "-";
    }
    cout << endl;
}

void randNum(int a[], int size){
    for(int i = 0; i < size; i++){
        a[i] = rand() % 100;
    }
}

void arrayToFile(string fileName, int*&intArray, int size)
{
    fstream file;
    file.open(fileName, ios::out | ios::binary);
    file.write(reinterpret_cast<char*>(&intArray), size);
    file.close();
    
    cout << "array to file: complete" << endl;
}

void fileToArray(string fileName, int*&newArray, int size)
{
    fstream file;
    file.open(fileName, ios::in | ios::binary);
    file.read(reinterpret_cast<char*>(&newArray), size);
    file.close();
    
    cout << "file to array: complete" << endl;
}