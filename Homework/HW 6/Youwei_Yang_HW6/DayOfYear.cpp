/* 
 * File:   DayOfYear.cpp
 * Author: Stanley
 * 
 * Created on October 15, 2016, 10:52 PM
 */

#include "DayOfYear.h"
#include <string>

DayOfYear::DayOfYear()
{
    day = 0;
    month = 0;
}

DayOfYear::DayOfYear(string month, int day)
{
    int monthList[] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    string months[] = {"January", "February", "March", "April", "May", "June", "July",
                        "August", "September", "October", "November", "December"};
    
    bool monthValid = false;
    
    for(int i = 0; i < 12; i++)
    {
        if(month == months[i])
        {
            this->month = i;
        }
    }
    
    if(!(day <= monthList[this->month]))
    {
        cout << "INVALID INPUT!" << endl;
        exit(1);
    }
    
    for(int i = 0; i < this->month; i++)
    {
        this->day += monthList[i];
    }
    this->day += day;
    
    dayLeft = this->day;
    
    for(int i = 0; i < this->month; i++)
    {
        dayLeft = dayLeft - monthList[i];
        
        if(dayLeft == 0)
        {
            dayLeft = monthList[this->month-1];
            this->month = this->month - 1;
        }
    }
}

DayOfYear::DayOfYear(const DayOfYear& orig)
{
    this->day = orig.day;
    this->month = orig.month;
}

DayOfYear DayOfYear::operator++()
{
    ++day;
    
    int monthList[] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    dayLeft++;
    if(dayLeft > monthList[this->month])
    {
        dayLeft = 1;
        this->month = this->month + 1;
    }
    
    return *this;
}

DayOfYear DayOfYear::operator ++(int)
{
    DayOfYear temp(*this);
    ++day;
    
    int monthList[] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    dayLeft++;
    if(dayLeft > monthList[this->month])
    {
        dayLeft = 1;
        this->month = this->month + 1;
    }
    
    return temp;
}

DayOfYear DayOfYear::operator--()
{
    --day;
    
    int monthList[] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    dayLeft--;
    if(dayLeft == 0)
    {
        dayLeft = monthList[this->month-1];
        this->month = this->month - 1;
    }
    return *this;
}

DayOfYear DayOfYear::operator --(int)
{
    DayOfYear temp(*this);
    --day;
    
    int monthList[] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    dayLeft--;
    if(dayLeft == 0)
    {
        dayLeft = monthList[this->month-1];
        this->month = this->month - 1;
    }
    return temp;
}

DayOfYear::~DayOfYear()
{
    
}

void DayOfYear::setDay(int day)
{
    this->day = day;
}

string DayOfYear::printMonth(int month)
{
    string months[] = {"January", "February", "March", "April", "May", "June", "July",
                        "August", "September", "October", "November", "December"};
    
    return months[month];
}

void DayOfYear::print()
{
    cout << "Day " << day << " would be " << printMonth(month) << " " << dayLeft << endl;
}