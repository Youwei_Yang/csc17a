/* 
 * File:   FeetInches.h
 * Author: Stanley
 *
 * Created on October 16, 2016, 1:05 AM
 */

#ifndef FEETINCHES_H
#define	FEETINCHES_H

#include <iostream>

using namespace std;

class FeetInches
{
public:
    // Constructor
    FeetInches(int feet = 0, int inches = 0);
    FeetInches(const FeetInches& orig); // Copy constructor
    
    // Destructor
    virtual ~FeetInches();
    
    // Mutator functions
    void setFeet(int);
    void setInches(int);
    
    // Accessor function
    int getFeet();
    int getInches();
    
    // Overloaded Operator functions
    FeetInches operator+(const FeetInches &);
    FeetInches operator-(const FeetInches &);
    FeetInches operator++();
    FeetInches operator++(int);
    bool operator>(const FeetInches &);
    bool operator<(const FeetInches &);
    bool operator==(const FeetInches &);
    bool operator<=(const FeetInches &);
    bool operator>=(const FeetInches &);
    bool operator!=(const FeetInches &);
    
    // Friends
    friend ostream &operator << (ostream &, const FeetInches &);
    friend istream &operator >> (istream &, FeetInches &);
    
    // Other functions
    FeetInches mutiply(FeetInches &);
    
private:
    int feet;
    int inches;
    void simplify();
};

#endif	/* FEETINCHES_H */

