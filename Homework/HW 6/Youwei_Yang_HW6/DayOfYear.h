/* 
 * File:   DayOfYear.h
 * Author: Stanley
 *
 * Created on October 15, 2016, 10:52 PM
 */
#ifndef DAYOFYEAR_H
#define	DAYOFYEAR_H

#include <cstdlib>
#include <iostream>

using namespace std;

class DayOfYear
{
public:
    // Getters & Setters
    void setDay(int);
    
    // Constructor
    DayOfYear(); // Default constructor
    DayOfYear(string, int);
    DayOfYear(const DayOfYear& orig); // Copy constructor
    
    // prefix and postfix increment
    DayOfYear operator++();
    DayOfYear operator++(int);
    
    // prefix and postfix decrement
    DayOfYear operator--();
    DayOfYear operator--(int);
    
    // Destructor
    ~DayOfYear();
    
    // Operator
    string printMonth(int);
    int getMonth();
    void print();
    
private:
    int day;
    int dayLeft;
    int month;
};

#endif	/* DAYOFYEAR_H */