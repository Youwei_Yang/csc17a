/* 
 * File:   main.cpp
 * Author: Stanley
 *
 * Created on September 17, 2016, 8:32 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

void line();
void combineName(char*&, char*&, char*&, char*&);
int cstringCount(char []);
struct MovieData{
    string title;
    string director;
    int yearReleased;
    int runningTime;
    long productionCost;
    long firstYearRevenues;
};
void printMovieData(MovieData);
struct WeatherStatistics{
    double totalRainfall;
    double highTemp;
    double lowTemp;
    double averageTemp;
};

/*
 * 
 */
int main(int argc, char** argv) {

    bool run = true;
    
    while(run){
        cout << "HOMEWORK 3" << endl << endl;
        cout << "1) 10.13 Date Printer" << endl;
        cout << "2) 10.7 Name Arranger" << endl;
        cout << "3) 11.1 Movie Data & 11.2 Movie Profit" << endl;
        cout << "4) 11.4 Weather Statistics" << endl;
        cout << "5) Exit" << endl;
        cout << "Input: ";
        
        int input;
        cin >> input;
        
        line();
        
        switch(input)
        {
            //case 1: 10.13 DATE PRINTER
            case 1: 
            {
                cout << "10.13 DATE PRINTER" << endl << endl;
                cout << "Enter a date (mm/dd/yyyy): ";

                char dateInput[10];
                cin >> dateInput;

                string months[12] = 
                {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};

                int monthDigit = dateInput[1] - 48;
                int monthCount;

                if(dateInput[0] == '0'){
                    monthCount = monthDigit;
                }else{
                    monthCount = 10 + monthDigit;
                }

                string month = months[monthCount-1];
                string day = "";
                day += dateInput[3];
                day += dateInput[4];
                string year;
                for(int i = 6; i < 12; i++){
                    year += dateInput[i];
                }

                cout << month << " " << day << ", " << year << endl;
                break;
            }
            
            //case 2: 10.7 Name Arranger
            case 2:
            {
                cout << "10.7 Name Arranger" << endl << endl;
                char fnString[100];
                char mnString[100];
                char lsString[100];
                char fullString[100];
                cout << "First name: ";
                cin >> fnString;
                char*firstName = fnString;
                cout << "Middle name: ";
                cin >> mnString;
                char*middleName = mnString;
                cout << "Last name: ";
                cin >> lsString;
                char*lastName = lsString;
                char*fullName = new char[100];
                combineName(firstName, middleName, lastName, fullName);
                cout << "Full Name: ";
                for(int i = 0; i < cstringCount(fullName); i++){
                    cout << fullName[i];
                }
                cout << endl;
                
                break;
            }
            
            //case 3: 11.1 Movie Data
            case 3:
            {
                cout << "11.1 MOVIE DATA & 11.2 MOVIE PROFIT" << endl << endl;
                //creating the movie: Avengers
                MovieData avengers;
                avengers.title = "Avengers";
                avengers.director = "Joss Whedon";
                avengers.yearReleased = 2012;
                avengers.runningTime = 143;
                avengers.productionCost = 220000000;
                avengers.firstYearRevenues = 1520000000;
                
                //creating the movie: Captain America: Civil War
                MovieData finestHours;
                finestHours.title = "The Finest Hours";
                finestHours.director = "Craig Gillespie";
                finestHours.yearReleased = 2016;
                finestHours.runningTime = 117;
                finestHours.productionCost = 80000000;
                finestHours.firstYearRevenues = 52100000;
                
                printMovieData(avengers);
                cout << endl;
                printMovieData(finestHours);
                
                break;
            }
            
            //case 4: 11.4 Weather Statistics
            case 4:
            {
                cout << "11.4 WEATHER STATISTICS" << endl << endl;
                WeatherStatistics*ws = new WeatherStatistics[12];
                
                //input data
                for(int i = 0; i < 12; i++)
                {
                    cout << "Month: " << i + 1 << endl;
                    cout << "Total Rainfall: ";
                    cin >> ws[i].totalRainfall;
                    cout << "High Temperature: ";
                    cin >> ws[i].highTemp;
                    cout << "Low Temperature: ";
                    cin >> ws[i].lowTemp;
                    cout << "Average Temperature: ";
                    cin >> ws[i].averageTemp;
                }
                
                //calculating data
                double totalRainfall;
                double highestTemp = ws[0].highTemp;
                double lowestTemp = ws[0].lowTemp;
                double totalTemp;
                
                for(int i = 0; i < 12; i++){
                    totalRainfall += ws[i].totalRainfall;
                    totalTemp += ws[i].averageTemp;
                    if(ws[i].highTemp > highestTemp)
                    {
                        highestTemp = ws[i].highTemp;
                    }
                    if(ws[i].lowTemp < lowestTemp)
                    {
                        lowestTemp = ws[i].lowTemp;
                    }
                }
                
                cout << endl;
                
                //output data
                cout << "Total rainfall: " << totalRainfall << endl;
                cout << "Average monthly rainfall: " << totalRainfall/12 << endl;
                cout << "Highest temperature: " << highestTemp << endl;
                cout << "Lowest temperature: " << lowestTemp << endl;
                cout << "Average monthly temperature: " << totalTemp/12 << endl;
                
                break;
            }
            
            //case 5: Exit
            case 5:
            {
                return 0;
            }
        }
        line();
    }
    
    return 0;
}

void line()
{
    for(int i = 0; i < 40; i++)
    {
        cout << "-";
    }
    
    cout << endl;
}

/**
 * Count the number of characters in a C-String
 * @param c The C-String
 * @return An int of number of characters
 */
int cstringCount(char c[]){
    int count = 0;
    while(c[count] != '\0'){
        count++;
    }
    
    return count;
}

/**
 * Combines the three C-String into one C-String
 * @param fn First name C-String
 * @param mn Middle Name C-String
 * @param ln Last Name C-String
 * @param full Full Name C-String
 */
void combineName(char*&fn, char*&mn, char*&ln, char*&full){
    char*temp = new char[100];
    int count = 0;
    
    for(int i = 0; i < cstringCount(ln); i++){
        temp[count] = ln[i];
        count++;
    }
    temp[count] = ',';
    count++;
    temp[count] = ' ';
    count++;
    
    for(int i = 0; i < cstringCount(fn); i++){
        temp[count] = fn[i];
        count++;
    }
    temp[count] = ' ';
    count++;
    
    for(int i = 0; i < cstringCount(mn); i++){
        temp[count] = mn[i];
        count++;
    }
    
    delete[] full;
    full = temp;
}

/**
 * Print out all information regarding the movie
 * @param m The movie
 */
void printMovieData(MovieData m){
    int mHours = (m.runningTime)/60;
    int mMinutes = (m.runningTime) - (mHours*60);
    
    cout << "Title: " << m.title << endl;
    cout << "Director: " << m.director << endl;
    cout << "Release date: " << m.yearReleased << endl;
    cout << "Running time: " << mHours << "h " << mMinutes << "m" << endl;
    if(m.productionCost < m.firstYearRevenues){
        cout << "First year profit: ";
    } else {
        cout << "First year loss: ";
    }
    string numWithCommas = to_string(m.firstYearRevenues - m.productionCost);
    int insertPosition = numWithCommas.length() - 3;
    while (insertPosition > 0)
    {
        numWithCommas.insert(insertPosition, ",");
        insertPosition -=3;
    }
    cout << numWithCommas << endl;
}