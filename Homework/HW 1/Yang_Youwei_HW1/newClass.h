/* 
 * File:   newClass.h
 * Author: Stanley
 *
 * Created on September 3, 2016, 3:31 PM
 */

#ifndef NEWCLASS_H
#define	NEWCLASS_H

class newClass {
public:
    newClass();
    newClass(const newClass& orig);
    virtual ~newClass();
private:

};

#endif	/* NEWCLASS_H */

