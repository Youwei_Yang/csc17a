/* 
 * File:   main.cpp
 * Author: Youwei Yang
 * Class: CSC17A
 * HW# 1
 *
 * Created on September 2, 2016, 5:07 PM
 */

#include <cstdlib>
#include <iostream>
#include <string>
#include <fstream>

using namespace std;

void randNum(int [], int);
void printIntArray(int [], int);
string*deleteEntry(string*, int, int);
void printStringArray(string [], int);
string coinToss(int);
void ascendingSort(int [], int);
int average(int [], int);
int*dropFirstEntry(int*, int);

/*
 * 
 */
int main(int argc, char** argv) {

    //initiate random seed
    srand(time(NULL));
    
    //-------------------------------HW1 Problem 1-------------------------------
    cout << "HW1 - 1" << endl;
    int size = 10;
    int*p = new int[size];
    randNum(p, size);
    printIntArray(p, size);
    
    //-------------------------------HW1 Problem 2-------------------------------
    cout << endl << "HW1 - 2: Removing values from a dynamic array" << endl;
    string names[5] = {"Stanley", "Leanne", "Amanda", "Nick", "Sam"};
    string*name = names;
    printStringArray(name, 5);
    name = deleteEntry(name, 5, 3);
    printStringArray(name, 5);
    
    //-------------------------------HW1 Problem 3-------------------------------
    cout << endl << "HW1 - 3: Coin Toss" << endl;
    int input;
    cout << "Number of toss: ";
    cin >> input;
    cout << coinToss(input);
    
    //-------------------------------HW1 Problem 4-------------------------------
    cout << endl << endl << "HW1 - 4: Test Scores #1" << endl;
    int numberOfTests;
    int testScore;
    cout << "Number of test total: ";
    cin >> numberOfTests;
    int*score = new int[numberOfTests];
    for(int i = 0; i < numberOfTests; i++){
        cout << "Test " << i + 1 << " score: ";
        cin >> testScore;
        score[i] = testScore;
    }
    cout << "Before sort: ";
    printIntArray(score, numberOfTests);
    ascendingSort(score, numberOfTests);
    cout << "After ascending sort: ";
    printIntArray(score, numberOfTests);
    cout << "Average score: " << average(score, numberOfTests);
    
    //-------------------------------HW1 Problem 5-------------------------------
    numberOfTests--;
    cout << endl << endl << "HW1 - 5: Modify Problem 4" << endl;
    delete[] score;
    score = dropFirstEntry(score, numberOfTests);
    cout << "After lowest score dropped: ";
    printIntArray(score, numberOfTests);
    cout << "Average score after drop: " << average(score, numberOfTests);
    
    //-------------------------------HW1 Problem 6-------------------------------
    cout << endl << endl << "HW1 - 6: Modify Problem 5" << endl;
    string fileName = "score.txt";
    //calculate the number of lines from the fine
    int number = 0;
    int numberOfLines;
    ifstream fin (fileName);
    while(fin>>number){
        ++numberOfLines;
    }
    fin.close();
    //output the average
    int*finScore = new int [numberOfLines];
    int i = 0;
    ifstream fin2 (fileName);
    while(fin2>>number){
        finScore[i] = number;
        i++;
    }
    fin2.close();
    cout << "Scores from the file " << fileName << " : ";
    printIntArray(finScore, numberOfLines);
    cout << "After ascending sort: ";
    printIntArray(finScore, numberOfLines);
    cout << "After lowest score dropped: ";
    printIntArray(finScore, numberOfLines);
    cout << "Average score after drop: " << average(finScore, numberOfLines);
    
    return 0;
}

/**
 * Insert random number to an array
 * @param a The array
 * @param size The size of the array
 */
void randNum(int a[], int size){
    for(int i = 0; i < size; i++){
        a[i] = rand() % 100;
    }
}

/**
 * Print an array of int
 * @param a The array
 * @param size The size of the array
 */
void printIntArray(int a[], int size){
    for(int i = 0; i < size; i++){
        cout << a[i] << " ";
    }
    cout << endl;
}

/**
 * Delete an entry at selected location
 * @param array The dynamic array
 * @param size The size of the array
 * @param loc The location to be deleted
 * @return 
 */
string*deleteEntry(string*array, int size, int loc){
    string*newArray = new string[size - 1];
    int j = 0;
    
    for(int i = 0; i < size; i++){
        if(i != loc){
            newArray[j] = array[i];
            j++;
        }
    }
    return newArray;
}

/**
 * Print an array of string
 * @param s An array of String
 * @param size The size of the array
 */
void printStringArray(string s[], int size){
    for(int i = 0; i < size; i++){
        cout << s[i] << " ";
    }
    cout << endl;
}

/**
 * Toss coin selected amount of times and print the result
 * @param n The number of times to toss
 * @return a The string to include the result
 */
string coinToss(int n){
    string result = "";
    for(int i = 0; i < n; i++){
        int toss = rand() % 2 + 1;
        if(toss == 1){
            result += "head ";
        } else {
            result += "tail ";
        }
    }
    return result;
}

/**
 * Sort an dynamic array in ascending order
 * @param array The array
 * @param size The size of the array
 * @return A sorted array
 */
 void ascendingSort(int array[], int size){
    int a;
    for(int i = 0; i < size; i++){
        for(int j = 0; j < size; j++){
            if( array[i] < array[j]){
                a = array[i];
                array[i] = array[j];
                array[j] = a;
            }
        }
    }
}
 
 /**
  * Return the average of the array elements
  * @param array The array
  * @param size The array size
  * @return An int containing the average
  */
 int average(int array[], int size){
     int result = 0;
     for(int i = 0; i < size; i++){
         result += array[i];
     }
     return result/size;
 }
 
 /**
  * Drop the first entry of an array of int
  * @param array The array
  * @param size The size of the array
  * @return An array with first entry dropped
  */
 int*dropFirstEntry(int*array, int size){
     int*newArray = new int[size];
     
     for(int i = 0; i < size; i++){
         newArray[i] = array[i+1];
     }
     return newArray;
 }