/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on September 22, 2016, 7:44 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

class RetailItem
{
private:
    string description;
    int unitsOnHand;
    double price;
public:
    // Setters
    void setDescription(string);
    void setUnitsOnHand(int);
    void setPrice(double);
    
    // Getters
    string getDescription();
    int getUnitsOnHand();
    double getPrice();
    
    // Constructor
    RetailItem();
    RetailItem(string, int, double);
};

class TestScores
{
private:
    double testScore;
};

void printRetailItem(RetailItem);
void line();

/*
 * 
 */
int main(int argc, char** argv) {

    bool run = true;
    
    while(run)
    {
        cout << "Lab Week 4" << endl;
        cout << "1) 13.5 Retail Item Class" << endl;
        cout << "2) 13.7 TestScores Class" << endl;
        cout << "Input: ";
        int input;
        cin >> input;
        line();
        
        switch(input)
        {
            case 1: // 13.5 Retail Item Class
            {
                cout << "13.5 Retail Item Class" << endl;
                
                // Create Items
                RetailItem item1("Jacket", 12, 59.95);
                RetailItem item2("Designer Jeans", 40, 34.95);
                RetailItem item3("Shirt", 20, 24.95);
                
                // Output items
                cout << "Item#1" << endl;
                printRetailItem(item1);
                cout << endl << "Item#2" << endl;
                printRetailItem(item2);
                cout << endl << "Item#2" << endl;
                printRetailItem(item3);
                
                break;
            }
            
            case 2: // 13.7 TestScores Class
            {
                cout << "13.7 TestScores Class" << endl;
                
                
            }
        }
        
        line();
    }
    
    return 0;
}

void RetailItem::setDescription(string description)
{
    this->description = description;
}

void RetailItem::setUnitsOnHand(int unitsOnHand)
{
    this->unitsOnHand = unitsOnHand;
}

void RetailItem::setPrice(double price)
{
    this->price = price;
}

string RetailItem::getDescription()
{
    return description;
}

int RetailItem::getUnitsOnHand()
{
    return unitsOnHand;
}

double RetailItem::getPrice()
{
    return price;
}

RetailItem::RetailItem()
{
    description = "...";
    unitsOnHand = 0;
    price = 0.00;
}

RetailItem::RetailItem(string description, int unitsOnHand, double price)
{
    this->description = description;
    this->unitsOnHand = unitsOnHand;
    this->price = price;
}

void printRetailItem(RetailItem item)
{
    cout << "Description: " << item.getDescription() << endl;
    cout << "Units On Hand: " << item.getUnitsOnHand() << endl;
    cout << "Price: " << item.getPrice() << endl;
}

void line()
{
    for(int i = 0; i < 40; i ++)
    {
        cout << "-";
    }
    cout << endl;
}