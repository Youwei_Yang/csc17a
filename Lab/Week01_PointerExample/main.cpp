/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on September 1, 2016, 7:17 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

//Function prototype
void outputArray(int [], int);

/*
 * 
 */
int main(int argc, char** argv) {
    
    //seed random value
    srand(time(0));

    //Create a single pointer
    int*p;
    
    int x = 12;
    
    //use the "address-of operator (&)
    
    p = &x;
    
    cout << "Value of p (address of x): " << p << endl;
    cout << "Value contained within p: " << *p << endl;
    cout << "Value in x: " << x << endl;
    
    //Dereference p by using indirection operator
    *p = 30;
    
    cout << "Value of p (address of x): " << p << endl;
    cout << "Value contained within p: " << *p << endl;
    cout << "Value in x: " << x << endl;
    
    //Create a dynamic array using new
    int size = 10;
    int *ap = new int[size];
    
    outputArray(ap, size);
    
    //store random values
    for (int i = 0; i < size; i++){
        ap[i] = rand() % 100;
    }
    
    outputArray(ap, size);
    
    return 0;
}

//Example of Doxygen commenting

/**
 * This function outputs the contents of an\n array
 * @param a The array
 * @param size The size of the array
 */
void outputArray(int a[], int size){
    for(int i = 0; i < size; i++){
        cout << a[i] << " ";
    }
    cout << endl;
}

