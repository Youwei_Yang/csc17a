/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on September 15, 2016, 7:28 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

struct Address
{
    string address;
    string city;
    string state;
    int zip;
};

struct date
{
    int month;
    int day;
    int year;
};

struct CustomerAccount
{
    string name;
    Address address;
    long telephoneNumber;
    double accountBalance;
    date dateOfLastPayment;
};

void line();
void print(CustomerAccount [], int);

/*
 * 
 */
int main(int argc, char** argv) {

    bool run = true;
    
    while (run){
        int size = 10;
        int count = 0;
        CustomerAccount ca[size];

        cout << "CUSTOMER ACCOUNTS" << endl;
        cout << "1. Add new customer" << endl;
        cout << "2. Search customer" << endl;
        cout << "3. Exit" << endl;
        cout << "Input: ";

        int input;

        cin >> input;
        line();
        if(input == 1){
            string stringInput;
            
            cout << "ADD NEW CUSTOMER" << endl;
            cout << "Name: ";
            cin >> ca[count].name;
            cin.ignore();
            cout << "Address: ";
            getline(cin, ca[count].address.address);
            cout << "City: ";
            cin.ignore();
            getline(cin, ca[count].address.city);
            cout << "State: ";
            cin >> ca[count].address.state;
            cout << "ZIP: ";
            cin >> ca[count].address.zip;
            cout << "Telephone Number: ";
            cin >> ca[count].telephoneNumber;
            cout << "Account Balance: ";
            cin >> ca[count].accountBalance;
            cout << endl << "DATE OF LAST PAYMENT";
            cout << "Month: ";
            cin >> ca[count].dateOfLastPayment.month;
            cout << "Day: ";
            cin >> ca[count].dateOfLastPayment.day;
            cout << "Year: ";
            cin >> ca[count].dateOfLastPayment.year;
            
            count++;
        }
        else if(input == 2)
        {
            string search;
            
            cout << "SEARCH CUSTOMER" << endl;
            cout << "Input the name of the customer: ";
            cin >> search;
            
            for(int i = 0; i < count; i++)
            {
                if(search == ca[i].name)
                {
                    print(ca, i);
                }
                else if
                {
                    cout << "No search result found";
                }
            }
        }
        else if(input == 3)
        {
            run = false;
        }
        else
        {
            cout << "Please only enter number 1 ~ 3" << endl;
            line();
        }
    }
    
    return 0;
}

void line()
{
    for(int i = 0; i < 50; i++)
    {
        cout << "-";
    }
    
    cout << endl;
}

void print(CustomerAccount s[], int loc)
{
    cout << "Name: " << s[loc].name << endl;
    cout << "Address: " << s[loc].address.address
            << ", " << s[loc].address.city
            << ", " << s[loc].address.state
            << " " << s[loc].address.zip << endl;
    cout << "Telephone Number: " << s[loc].telephoneNumber;
    cout << "Account Balance: $" << s[loc].accountBalance;
    cout << "Date of Last Payment: " << s[loc].dateOfLastPayment.day
            << "/" << s[loc].dateOfLastPayment.month
            << "/" << s[loc].dateOfLastPayment.year << endl;
}