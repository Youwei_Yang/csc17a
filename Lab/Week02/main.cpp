/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on September 8, 2016, 7:42 PM
 */

#include <cstdlib>
#include <iostream>
#include <cstdlib>

using namespace std;

void randNum(int [], int);
void printIntArray(int [], int);
void increaseByOne(int*&, int&);
int wordCounter (char []);
/*
 * 
 */
int main(int argc, char** argv) {

    //initiate random seed
    srand(time(NULL));
    
    //-------------------------------9.12 Element Shifter-----------------------
    cout << "9.12 Element Shifter" << endl;
    
    int size = 5;
    int*arguArray = new int[size];
    randNum(arguArray, size);
    cout << "Original array: ";
    printIntArray(arguArray, size);
    increaseByOne(arguArray, size);
    cout << "Increased by one: ";
    printIntArray(arguArray, size);
    
    //-------------------------------10.3 Word Counter--------------------------
    cout << "10.3 Word Counter" << endl;
    
    string userString;
    cout << "Input a string: ";
    cin >> userString;
    char*c = new char[userString];
    cout << "Word Count: " << wordCounter(c);
    
    return 0;
}

/**
 * Insert random number to an array
 * @param a The array
 * @param size The size of the array
 */
void randNum(int a[], int size){
    for(int i = 0; i < size; i++){
        a[i] = rand() % 100;
    }
}

/**
 * Print an array of int
 * @param a The array
 * @param size The size of the array
 */
void printIntArray(int a[], int size){
    for(int i = 0; i < size; i++){
        cout << a[i] << " ";
    }
    cout << endl;
}

void increaseByOne(int*&argu, int&size){
    int*newArray = new int[size + 1];
    newArray[0] = 0;
    for(int i = 0; i < size; i++){
        newArray[i+1] = argu[i];
    }
    delete[] argu;
    argu = newArray;
    size++;
}

int wordCounter (char c[]){
    int count = 0;
    int wordCount = 0;
    while(c[count]!='\0'){
        if(!isalpha[count]){
            wordCount++;
        }
        count++;
    }
    return wordCount;
}