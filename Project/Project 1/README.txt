{\rtf1\ansi\ansicpg1252\cocoartf1504\cocoasubrtf600
{\fonttbl\f0\fswiss\fcharset0 Helvetica;}
{\colortbl;\red255\green255\blue255;}
{\*\expandedcolortbl;\csgray\c100000;}
\margl1440\margr1440\vieww10800\viewh8400\viewkind0
\pard\tx720\tx1440\tx2160\tx2880\tx3600\tx4320\tx5040\tx5760\tx6480\tx7200\tx7920\tx8640\pardirnatural\partightenfactor0

\f0\fs24 \cf0 Youwei Yang - Project 1\
\
CHANGES:\
- automatic time record -> manual time record\
	* this is because I was using Mac OS, and the Time.h class I was planning to use doesn\'92t 	  support Mac\
\
ADDED:\
- option to create new user\
- out put file in the format of firstName_lastName_TimeSheet.txt\
- conformation message when user making important changes}