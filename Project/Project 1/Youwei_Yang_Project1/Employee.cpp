/* 
 * File:   Employee.cpp
 * Author: Stanley
 * 
 * Created on October 23, 2016, 3:00 AM
 */

#include "Employee.h"
#include <ostream>
#include <fstream>

/**
 * Default constructor
 */
Employee::Employee()
{
    firstName = "";
    lastName = "";
    size = 0;
    checkInTime = NULL;
    checkOutTime = NULL;
    sessionTime = NULL;
}

/**
 * Constructor that takes two strings: firstName and lastName
 * @param firstName is a string that will be set to this->firstName
 * @param lastName is a string that will be set to this->lastName
 */
Employee::Employee(string firstName, string lastName)
{
    this->firstName = firstName;
    this->lastName = lastName;
    size = 0;
    checkInTime = NULL;
    checkOutTime = NULL;
    sessionTime = NULL;
}

/**
 * Copy constructor that copies the firstName and lastName from orig
 * @param orig the object to be copy from
 */
Employee::Employee(const Employee& orig)
{
    firstName = orig.firstName;
    lastName = orig.lastName;
}

/**
 * Destructor that deletes three dynamic arrays: checkInTie, checkOutTime, sessionTime
 */
Employee::~Employee()
{
    delete[] checkInTime;
    delete[] checkOutTime;
    delete[] sessionTime;
}

/**
 * Set a string to this->firstName
 * @param firstName is the string which will be set to this->firstName
 */
void Employee::setFirstName(string firstName)
{
    this->firstName = firstName;
}

/**
 * Set a string to this->lastName
 * @param lastName is the string which will be set to this->lastName
 */
void Employee::setLastName(string lastName)
{
    this->lastName = lastName;
}

/**
 * Set the checking time
 * @param hour is the hour checked in
 * @param minute is the minute checked in
 */
void Employee::setCheckInTime(int hour, int minute)
{
    if(size == 0)
    {
        increaseSize();
        checkInTime[size-1].setHour(hour);
        checkInTime[size-1].setMinute(minute);
        isCheckedIn = true;
        cout << "CHECK IN: DONE" << endl;
    } else {
        if(isCheckedIn == true)
        {
            cout << "ERROR: ALREADY CHECKED IN" << endl;
        } else
        {
            increaseSize();
            checkInTime[size-1].setHour(hour);
            checkInTime[size-1].setMinute(minute);
            isCheckedIn = true;
            cout << "CHECK IN: DONE" << endl;
        }
    }
}

/**
 * Set the checkout time
 * @param hour is the hour checked out
 * @param minute is the minute checked out
 */
void Employee::setCheckOutTime(int hour, int minute)
{
    if(isCheckedIn ==  false)
    {
        cout << "ERROR: WERE NOT CHECKED IN" << endl;
    } else
    {
        checkOutTime[size-1].setHour(hour);
        checkOutTime[size-1].setMinute(minute);
        isCheckedIn = false;
        cout << "CHECK OUT: DONE" << endl;
    }
}

/**
 * Set the total time checked in from that one session
 */
void Employee::setSessionTime()
{
    int sessionHour = 0;
    int sessionMinute = 0;
    
    sessionHour = checkOutTime[size-1].getHour() - checkInTime[size-1].getHour();
    
    if(sessionHour < 0)
    {
        sessionHour = sessionHour + 24;
    } else if(sessionHour == 0)
    {
        sessionHour = 24;
    }
    
    sessionMinute = checkOutTime[size-1].getMinute() - checkInTime[size-1].getMinute();
    if(sessionMinute < 0)
    {
        sessionMinute = sessionMinute + 60;
        sessionHour--;
    }
    sessionTime[size-1].setMinute(sessionMinute);
    sessionTime[size-1].setHour(sessionHour);
}

/**
 * Gets the total time checking in from all sessions
 */
void Employee::getTotalTime()
{
    int totalHour = 0;
    int totalMinute = 0;
    
    for(int i = 0; i < size; i++)
    {
        totalHour = totalHour + sessionTime[i].getHour();
        totalMinute = totalMinute + sessionTime[i].getMinute();
    }
    
    int counter = totalMinute / 60;
    
    for(int i = 0; i < counter; i++)
    {
        totalHour++;
        totalMinute = totalMinute - 60;
    }
    
    cout << to_string(totalHour) + " hours, " + to_string(totalMinute) + " minutes" << endl;
}

/**
 * Returns user's first name
 * @return user's first name in a string
 */
string Employee::getFirstName()
{
    return firstName;
}

/**
 * Returns user's last name
 * @return user's last name in a string
 */
string Employee::getLastName()
{
    return lastName;
}

/**
 * return whether user is still checked in
 * @return a bool, with true if user is still checked in, and false if not
 */
bool Employee::getIsCheckedIn()
{
    return isCheckedIn;
}

/**
 * Deletes all three dynamic arrays: checkInTie, checkOutTime, sessionTime
 */
void Employee::reset()
{
    delete[] checkInTime;
    delete[] checkOutTime;
    delete[] sessionTime;
}

/**
 * Prints the result of all sessions 
 */
void Employee::printAll()
{
    setSessionTime();
    
    for(int i = 0; i < size; i++)
    {
        cout << to_string(checkInTime[i].getHour()) + ":" + to_string(checkInTime[i].getMinute())
                << " ~ "
                << to_string(checkOutTime[i].getHour()) + ":" + to_string(checkOutTime[i].getMinute()) << endl;
        cout << "Total of: "
                << to_string(sessionTime[i].getHour()) + "hours, " + to_string(sessionTime[i].getMinute()) + " minutes"<< endl;
        cout << endl;
    }
    
    cout << "Total  time: ";
    getTotalTime();
    cout << endl;
}

/**
 * Output all results from all sessions into a .txt file
 */
void Employee::toFile()
{
    string fileName = getFirstName() + "_" + getLastName() + "_TimeSheet.txt";
    ofstream ofile;
    ofile.open(fileName);
    for(int i = 0; i < size; i++)
    {
        ofile << to_string(checkInTime[i].getHour()) + ":" + to_string(checkInTime[i].getMinute())
                << " ~ "
                << to_string(checkOutTime[i].getHour()) + ":" + to_string(checkOutTime[i].getMinute()) << endl;
        ofile << "Total of: "
                << to_string(sessionTime[i].getHour()) + "hours, " + to_string(sessionTime[i].getMinute()) + " minutes"<< endl;
        ofile << endl;
    }
    
    ofile << "Total combined time: ";
    getTotalTime();
    ofile << endl;
    
    ofile.close();
}

/**
 * Increase the number of sessions
 */
void Employee::increaseSize()
{
    if(size == 0)
    {
        size ++;
        checkInTime = new Time[size];
        checkOutTime = new Time[size];
        sessionTime = new Time[size];
    } else
    {
        size++;

        Time*checkInTemp = new Time[size];
        for(int i = 0; i < size; i++)
        {
            checkInTemp[i] = checkInTime[i];
        }

        Time*checkOutTemp = new Time[size];
        for(int i = 0; i < size; i++)
        {
            checkOutTemp[i] = checkOutTime[i];
        }

        Time*sessionTemp = new Time[size];
        for(int i = 0; i < size; i++)
        {
            sessionTemp[i] = sessionTime[i];
        }

        reset();

        checkOutTime = checkOutTemp;
        checkInTime = checkInTemp;
        sessionTime = sessionTemp;
    }
}