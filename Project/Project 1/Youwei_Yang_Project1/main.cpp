/* 
 * File:   main.cpp
 * Author: Stanley
 *
 * Created on October 21, 2016, 12:46 AM
 */

#include <cstdlib>
#include <iostream>
#include <string>

#include "Employee.h"

using namespace std;

void line();

/*
 * 
 */
int main(int argc, char** argv) {

    int menuInput;
    int size = 10;
    int employeeCounter = 0;
    Employee*employees = new Employee[size];
    
    bool run = true;
    while(run){
        cout << "TIME SHEET" << endl << endl;
        cout << "1) Log In" << endl
                << "2) New User" << endl
                << "3) Exit" << endl
                << "Input: ";
        cin >> menuInput;

        line();

        switch(menuInput)
        {
            case 1:
            {
                int userLoc;
                string firstName;
                string lastName;
                bool userFound = false;
                
                // Log in && log in check
                cout << "Please enter your first name: ";
                cin >> firstName;
                for(int i = 0; i < size; i++)
                {
                    if(employees[i].getFirstName() == firstName)
                    {
                        userLoc = i;
                        userFound = true;
                        
                        for(int j = i + 1; j < size; j++)
                        {
                            if(employees[j].getFirstName() == firstName)
                            {
                                line();
                                cout << "ERROR: DUPLICATE FIRST NAME FOUND" << endl;
                                userLoc = NULL;
                                userFound = false;
                                j = size;
                                i = size;
                            }
                        }
                    }
                }
                if(userFound == false)
                {
                    line();
                    cout << "Please enter your last name: ";
                    cin >> lastName;
                    
                    for(int i = 0; i < size; i++)
                    {
                        if(employees[i].getLastName() == lastName)
                        {
                            userLoc = i;
                            userFound = true;
                        }
                    }
                }
                if(userFound == false)
                {
                    line();
                    cout << "ERROR: LOG IN FAIL" << endl;
                    break;
                }
                
                // Continue with success log in
                
                bool case1Run = true;
                int hour = 0;
                int minute = 0;
                
                while(case1Run)
                {
                    line();
                    
                    // Print menu
                    int case1Input;
                    cout << "1) Check in" << endl
                            << "2) Check out" << endl
                            << "3) Output to a file" << endl
                            << "4) Rest time sheet" << endl
                            << "5) Back to main menu" << endl
                            << "Input:";
                    cin >> case1Input;

                    switch(case1Input)
                    {
                        
                        case 1:
                        {
                            line();
                            
                            cout << "Enter the check in time below in 24Hour format" << endl;
                            cout << "Hour: ";
                            cin >> hour;
                            cout << "Minute: ";
                            cin >> minute;
                            
                            line();
                            
                            if((hour < 0 || hour > 24) || (minute < 0 || minute > 60))
                            {
                                cout << "ERROR: INPUT ERROR" << endl;
                                break;
                            }
                            
                            employees[userLoc].setCheckInTime(hour, minute);

                            break;
                        }

                        case 2:
                        {
                            line();
                            
                            cout << "Enter the check out time below in 24Hour format" << endl;
                            cout << "Hour: ";
                            cin >> hour;
                            cout << "Minute: ";
                            cin >> minute;
                            
                            line();
                            
                            if((hour < 0 || hour > 24) || (minute < 0 || minute > 60))
                            {
                                cout << "ERROR: INPUT ERROR" << endl;
                                break;
                            }
                            
                            employees[userLoc].setCheckOutTime(hour, minute);

                            break;
                        }

                        case 3:
                        {
                            line();
                            
                            if(employees[userLoc].getIsCheckedIn() == false)
                            {
                                employees[userLoc].printAll();
                                employees[userLoc].toFile();
                                cout << "FILE OUT PUT: DONE" << endl;
                            } else
                            {
                                cout << "ERROR: STILL CHECKED IN" << endl;
                            }
                            
                            break;
                        }

                        case 4:
                        {
                            line();
                            
                            string userInput;
                            char checkInput;

                            cout << "Confirm to reset the time sheet Y/N: ";
                            cin >> userInput;
                            checkInput = userInput.at(0);
                            
                            line();
                            
                            if(tolower(checkInput) == 'y')
                            {

                                employees[userLoc].reset();
                                cout << "TIME SHEET RESET: DONE" << endl;
                                
                                break;
                            } else if(tolower(checkInput) == 'n')
                            {
                                cout << "TIME SHEET RESET: CANCELED";
                                break;
                            } else
                            {
                                cout << "ERROR: WRONG INPUT" << endl;
                                break;
                            }
                        }

                        case 5:
                        {
                            case1Run = false;
                            break;
                        }
                    }
                }

                break;
            }

            case 2:
            {
                string userInput;
                char checkInput;
                string firstName;
                string lastName;
                
                cout << "Confirm to create a new user Y/N: ";
                cin >> userInput;
                checkInput = userInput.at(0);
                if(tolower(checkInput) == 'y')
                {
                    line();
                    
                    cout << "First Name: ";
                    cin >> firstName;
                    cout << "Last Name: ";
                    cin >> lastName;
                    employees[employeeCounter].setFirstName(firstName);
                    employees[employeeCounter].setLastName(lastName);
                    employeeCounter++;
                    
                    line();
                    break;
                } else if(tolower(checkInput) == 'n')
                {
                    line();
                    break;
                } else
                {
                    line();
                    cout << "ERROR: WRONG INPUT" << endl;
                    line();
                    break;
                }
            }

            case 3:
            {
                for(int i = 0; i < 10; i++)
                {
                    if(employees[i].getIsCheckedIn() == true)
                    {
                        cout << "ERROR: SOMEONE IS STILL CHECKED IN" << endl;
                        break;
                    } else{
                        return 0;
                    }
                }
                line();
            }
        }
    }
    return 0;
}

void line()
{
    for(int i = 0; i < 35; i++)
    {
        cout << "-";
    }
    
    cout << endl;
}