/* 
 * File:   Time.cpp
 * Author: Stanley
 * 
 * Created on October 23, 2016, 2:56 AM
 */

#include "Time.h"

/**
 * Default constructor
 */
Time::Time()
{
    minute = 0;
    hour = 0;
}

/**
 * Copy constructor
 * @param orig is the object to copy from
 */
Time::Time(const Time& orig)
{
    minute = orig.minute;
    hour = orig.hour;
}

/**
 * Destructor; nothing to delete
 */
Time::~Time()
{
    
}

/**
 * Set the minute of this object
 * @param minute is a int that will be set to this->minute
 */
void Time::setMinute(int minute)
{
    this->minute = minute;
}

/**
 * Set the hour of this object
 * @param hour is a int that will be set to this->hour
 */
void Time::setHour(int hour)
{
    this->hour = hour;
}

/**
 * Return the value of minute
 * @return the value of minute as an int
 */
int Time::getMinute()
{
    return minute;
}

/**
 * Return the value of hour
 * @return the value of hour as an int
 */
int Time::getHour()
{
    return hour;
}