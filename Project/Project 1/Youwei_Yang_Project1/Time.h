/* 
 * File:   Time.h
 * Author: Stanley
 *
 * Created on October 23, 2016, 2:56 AM
 */

#ifndef TIME_H
#define	TIME_H

#include <cstdlib>
#include <iostream>
#include <string>

using namespace std;

class Time
{
public:
    // Constructor
    Time(); // Default constructor
    Time(const Time& orig); // Copy constructor
    virtual ~Time(); // Destructor
    
    // Getter && Setter
    void setMinute(int minute);
    void setHour(int hour);
    int getMinute();
    int getHour();
    
private:
    int minute;
    int hour;
};

#endif	/* TIME_H */