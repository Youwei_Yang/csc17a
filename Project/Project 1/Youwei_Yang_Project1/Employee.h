/* 
 * File:   Employee.h
 * Author: Stanley
 *
 * Created on October 23, 2016, 3:00 AM
 */

#ifndef EMPLOYEE_H
#define	EMPLOYEE_H
#include "Time.h"

class Employee
{
public:
    // Constructor
    Employee(); // Default constructor
    Employee(string firstName, string lastName);
    
    // Copy constructor
    // Only copy firstName and lastName
    Employee(const Employee& orig);
    virtual ~Employee(); // Destructor
    
    // Getter && Setter
    void setFirstName(string firstName);
    void setLastName(string lastName);
    void setCheckInTime(int hour, int minute);
    void setCheckOutTime(int hour, int minute);
    void setSessionTime();
    void getTotalTime();
    string getFirstName();
    string getLastName();
    bool getIsCheckedIn();
    
    // Other Function
    void reset();
    void printAll();
    void toFile();
    void increaseSize();
    
private:
    string firstName;
    string lastName;
    int size = 0;
    bool isCheckedIn = false;
    Time*checkInTime;
    Time*checkOutTime;
    Time*sessionTime;
};

#endif	/* EMPLOYEE_H */