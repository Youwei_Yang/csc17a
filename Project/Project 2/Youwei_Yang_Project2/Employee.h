/* 
 * File:   Employee.h
 * Author: Stanley
 *
 * Created on December 5, 2016, 1:33 PM
 */

#ifndef EMPLOYEE_H
#define	EMPLOYEE_H
#include <cstdlib>
#include <iostream>
#include <string>

using namespace std;

class Employee
{
public:
    // Constructor
    Employee(string firstName, string lastName, int IDNum);
    Employee(const Employee& orig);
    virtual ~Employee();
    
    // Other Functions;
    void setFirstName(string firstName);
    void setLastName(string lastName);
    void setIDNum(int IDNum);
    string getFirstName();
    string getLastName();
    int getIDNum();
    
protected:
    string firstName;
    string lastName;
    int IDNum;
};

#endif	/* EMPLOYEE_H */