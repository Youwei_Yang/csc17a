/* 
 * File:   Time.cpp
 * Author: Stanley
 * 
 * Created on December 6, 2016, 1:09 PM
 */

#include "Time.h"

/**
 * Default Constructor
 */
Time::Time()
{
    minute = 0;
    hour = 0;
    day = 0;
    month = 0;
    year = 0;
}

/**
 * Constructor that creates a Time object with only minute and hour defined
 * @param minute
 * @param hour
 */
Time::Time(int minute, int hour)
{
    this->minute = minute;
    this->hour = hour;
    day = 0;
    month = 0;
    year = 0;
}

/**
 * Copy Constructor
 * @param orig the Time object to be copy from
 */
Time::Time(const Time& orig)
{
    minute = orig.minute;
    hour = orig.hour;
    day = orig.day;
    month = orig.month;
    year = orig.year;
}

/**
 * No Dynamic data to be deleted
 */
Time::~Time()
{
    
}

/**
 * change the minute of the object
 * @param minute is the value that will replace the existing minute
 */
void Time::setMinute(int minute)
{
    try
    {
        if(minute < 0)
            throw 1;
        this->minute = minute;
    }
    catch(int errorCode)
    {
        cout << "ERROR: Minute must be greater than 0" << endl;
    }
}

/**
 * change the hour of the object
 * @param hour is the value that will replace the existing hour
 */
void Time::setHour(int hour)
{
    try
    {
        if(hour < 0)
            throw 1;
        this->hour = hour;
    }
    catch(int errorCode)
    {
        cout << "ERROR: Hour must be greater than 0" << endl;
    }
}

/**
 * Change the day of the object
 * @param day is the value that will replace the existing day
 */
void Time::setDay(int day)
{
    try
    {
        if(day < 0)
            throw 1;
        this->day = day;
    }
    catch(int errorCode)
    {
        cout << "ERROR: Day must be greater than 0" << endl;
    }
}

/**
 * Change the month of the object
 * @param month is the value that will replace the existing month
 */
void Time::setMonth(int month)
{
    try
    {
        if(month < 0)
            throw 1;
        this->month = month;
    }
    catch(int errorCode)
    {
        cout << "ERROR: Month must be greater than 0" << endl;
    }
}

/**
 * Change the year of the object
 * @param year is the value that will replace the existing year
 */
void Time::setYear(int year)
{
    try
    {
        if(year < 0)
            throw 1;
        this->year = year;
    }
    catch(int errorCode)
    {
        cout << "ERROR: Year must be greater than 0" << endl;
    }
}

/**
 * @return an int of the minute of this object
 */
int Time::getMinute()
{
    return minute;
}

/**
 * @return an int of the hour of this object
 */
int Time::getHour()
{
    return hour;
}

/**
 * @return an int of the day of this object
 */
int Time::getDay()
{
    return day;
}

/**
 * @return an int of the month of this object
 */
int Time::getMonth()
{
    return month;
}

/**
 * @return an int of the year of this object
 */
int Time::getYear()
{
    return year;
}

/**
 * Set the current local time
 */
void Time::setCurrentTime()
{
    time_t t = time(0);   // get time now
    struct tm * now = localtime( & t );
    minute = now->tm_min;
    hour = now->tm_hour;
    day = now->tm_mday;
    month = now->tm_mon + 1;
    year = now->tm_year + 1900;
}

/**
 * @return a string of the dates in the format of YYYY-MM-DD
 */
string Time::dateToString()
{
    string result = to_string(year) + "-" + to_string(month) + "-" + to_string(day);
    return result;
}

/**
 * @return a string of the times in the format of HH:MM in 24 hours format
 */
string Time::timeToString()
{
    string result = to_string(hour) + ":" + to_string(minute);
    return result;
}