/* 
 * File:   TimeDynamicQueue.cpp
 * Author: Stanley
 * 
 * Created on December 11, 2016, 12:57 AM
 */

#include "TimeDynamicQueue.h"

/**
 * default constructor
 * set both front and rear to NULL
 * set numItems to 0
 */
TimeDynamicQueue::TimeDynamicQueue()
{
    front = rear = NULL;
    numItems = 0;
}

/**
 * Destructor
 */
TimeDynamicQueue::~TimeDynamicQueue()
{
    clear();
}

/**
 * add an TimeNode with three variables to the rear of the queue
 * @param checkinTime is the time user checked in
 * @param checkoutTime is the time user checked out
 * @param sessionTime is the total time user have checked in for
 */
void TimeDynamicQueue::enqueue(Time checkinTime, Time checkoutTime, Time sessionTime)
{
    try{
        TimeNode* newNode = new TimeNode();
        newNode->checkinTime = checkinTime;
        newNode->checkoutTime = checkoutTime;
        newNode->sessionTime = sessionTime;
        newNode->tail = NULL;
        if(isEmpty())
        {
            front = rear = newNode;
        }
        else
        {
            rear->tail = newNode;
            rear = newNode;
        }
        numItems++;
    }
    catch(bad_alloc)
    {
        cout << "ERROR: Cannot allocate memory" << endl;
        exit(EXIT_FAILURE);
    }
}

/**
 * delete an TimeNode from the front of the queue
 * @param x is where deleted TimeNode to be exported to
 */
void TimeDynamicQueue::dequeue(string& x)
{
    if(isEmpty())
        cout << "ERROR: Queue is empty" << endl;
    else
    {
        x = front->checkinTime.dateToString() + ": "
                + front->checkinTime.timeToString() + "~"
                + front->checkoutTime.timeToString();
                
        TimeNode* temp = front->tail;
        delete front;
        front = temp;
        numItems--;
    }
}

/**
 * delete everything in this dynamic queue
 */
void TimeDynamicQueue::clear()
{
    string temp;
    for(int i = 0; i < numItems; i++)
        dequeue(temp);
}

/**
 * determine whether the queue is empty
 * @return an boolean
 */
bool TimeDynamicQueue::isEmpty()
{
    return numItems <= 0;
}

/**
 * print every TimeNode within the queue
 */
void TimeDynamicQueue::printQueue()
{
    TimeNode* temp = front;
    
    while(temp != NULL)
    {
        cout << temp->checkinTime.dateToString()
                << ": "
                << temp->checkinTime.timeToString()
                << "~"
                << temp->checkoutTime.timeToString()
                << endl;
        
        temp = temp->tail;
    }
}