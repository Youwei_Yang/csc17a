/* 
 * File:   main.cpp
 * Author: Stanley
 *
 * Created on December 5, 2016, 1:29 PM
 */

#include <cstdlib>
#include <iostream>
#include <vector>
#include <fstream>
#include "PartTime.h"

using namespace std;

void line();
int mainMenu();
void case1(vector<PartTime>&);
int case1Menu(vector<PartTime>, int loc);
void case1Case6(vector<PartTime>&, int loc);
int case1Case6Menu(vector<PartTime>, int loc);
void case2(vector<PartTime>&);
void case3(vector<PartTime>&, bool&);
/*
 * 
 */
int main(int argc, char** argv) {

    // A vector that contains all users
    vector<PartTime> partTimes;
    
    bool run = true;
    while(run)
    {
        switch(mainMenu()) // Switch between the choices from mainMenu() function
        {
            case 1:
            {
                case1(partTimes);
                break;
            }
            
            case 2:
            {
                case2(partTimes);
                line();
                break;
            }
            
            case 3:
            {
                case3(partTimes, run);
                break;
            }
            
            default:
            {
                cout << "ERROR: INVALID INPUT, PLEASE TRY AGAIN" << endl;
                line();
            }
        }
    }
    
    return 0;
}

/**
 * this function prints the menu and return the user choice
 * @return the user input
 */
int mainMenu()
{
    int input;
    cout << "TIME SHEET" << endl 
            << "Note: To exit the program safely, please use option 3"
            << endl << endl;
    cout << "1. Login" << endl
            << "2. New User" << endl
            << "3. Exit" << endl
            << "Input: ";
    cin >> input;
    line();
    return input;
}

/**
 * This function will run the case 1 of the main menu
 * @param v is the vector that contains all users
 */
void case1(vector<PartTime>& v)
{
    int IDNum;
    int loc;
    cout << "Please enter your ID Number: ";
    cin >> IDNum;
    
    try
    {
        // check user exist
        for(int i = 0; i < v.size(); i++)
        {
            if(v[i].getIDNum() == IDNum) // if the user is found
            {
                throw i; // throw the location of the user
            }
        }
        
        // if not found; return back to the main menu
        line();
        cout << "ERROR: USER NOT FOUND" << endl;
        line();
        return;
    }
    catch (int userFound)
    {
        loc = userFound;;
    }
    
    bool run = true;
    while(run)
    {
        switch(case1Menu(v, loc))
        {
            case 1:
            {
                v[loc].checkin();
                break;
            }

            case 2:
            {
                v[loc].checkout();
                break;
            }

            case 3:
            {
                v[loc].print();
                break;
            }

            case 4:
            {
                v[loc].toFile();
                break;
            }

            case 5:
            {
                v[loc].clear();
                break;
            }
            
            case 6:
            {
                case1Case6(v, loc);
                break;
            }

            case 7:
            {
                run = false;
                break;
            }

            default:
            {
                cout << "ERROR: INVALID INPUT, PLEASE TRY AGAIN" << endl;
                break;
            }
        }
    }
}

/**
 * print and get the user input in the menu of case 1
 * @param v is the vector that contains all users
 * @param loc is the location of the user who is logged in
 * @return an int of user input
 */
int case1Menu(vector<PartTime> v, int loc)
{
    int input;
    line();
    cout << v[loc].getFirstName() <<" "
            << v[loc].getLastName() << " : "
            << v[loc].getIDNum() << endl << endl;
    cout << "1) Check in" << endl
        << "2) Check out" << endl
        << "3) Review time sheet" << endl
        << "4) Output to a file" << endl
        << "5) Rest time sheet" << endl
        << "6) Setting" << endl
        << "7) Log off" << endl
        << "Input:";
    cin >> input;
    line();
    return input;
}

/**
 * This function will run the case 6 of case 1
 * @param v is the vector that contains all of the users
 * @param loc is the location of the user who is logged in
 */
void case1Case6(vector<PartTime>& v, int loc)
{
    bool run = true;
    while(run)
    {
        switch(case1Case6Menu(v, loc))
        {
            case 1:
            {
                line();
                string firstName;
                cout << "Please enter the new first name: ";
                cin >> firstName;
                v[loc].setFirstName(firstName);
                line();
                cout << "FIRST NAME HAS BEEN CHANGED" << endl;
                line();
                break;
            }
            
            case 2:
            {
                line();
                string lastName;
                cout << "Please enter the new last name: ";
                cin >> lastName;
                v[loc].setLastName(lastName);
                line();
                cout << "LAST NAME HAS BEEN CHANGED" << endl;
                line();
                break;
            }
            
            case 3:
            {
                line();
                int IDNum;
                cout << "Please enter the new ID Number: ";
                cin >> IDNum;
                
                try
                {
                    // check if the same IDNum already exist
                    for(int i = 0; i < v.size(); i++)
                    {
                        if(v[i].getIDNum() == IDNum) // if the same ID Number was found
                        {
                            throw 1;
                        }
                    }
                    v[loc].setIDNum(IDNum);
                    line();
                    cout << "ID NUMBER HAS BEEN CHANGED" << endl;
                    line();
                    break;
                }
                catch(int errorCode)
                {
                    line();
                    cout << "ERROR: DULICATED ID NUMBER FOUND" << endl;
                    line();
                    break;
                }
            }
            
            case 4:
            {
                run = false;
                break;
            }
            
            default:
            {
                line();
                cout << "ERROR: INVALID INPUT, PLEASE TRY AGAIN" << endl;
                break;
            }
        }
    }
}

/**
 * Print the menu for case 1-6, will return the user choice
 * @param v is the vector that contains all of the users
 * @param loc is the location of the user who is logged in
 * @return an int of user's menu choice
 */
int case1Case6Menu(vector<PartTime> v, int loc)
{
    int input;
    cout << v[loc].getFirstName() <<" "
        << v[loc].getLastName() << " : "
        << v[loc].getIDNum() << endl << endl;
    cout << "1) Change first name" << endl
            << "2) Change last name" << endl
            << "3) Change ID Number" << endl
            << "4) Back" << endl
            << "Input: ";
    cin >> input;
    return input;
}

/**
 * this function provides the menu for creating a new user
 * will also add the new user to a vector
 * @param v is the vector that contains all the part time employees
 */
void case2(vector<PartTime>& v)
{
    string firstName;
    string lastName;
    int IDNum;

    cout << "Please enter the following info." << endl << endl;
    cout << "First Name: ";
    cin >> firstName;
    cout << "Last Name: ";
    cin >> lastName;
    cout << "ID Number: ";
    cin >> IDNum;
    
    // check if the same IDNum already exist
    for(int i = 0; i < v.size(); i++)
    {
        if(v[i].getIDNum() == IDNum) // if the same ID Number was found
        {
            cout << "ERROR: DULICATED ID NUMBER FOUND" << endl;
            return;
        }
    }
    
    PartTime temp(firstName, lastName, IDNum);
    v.push_back(temp);
}

/**
 * simply creates a line that makes the code looking more organized
 */
void line()
{
    for(int i = 0; i < 35; i++)
        cout << "-";
    cout << endl;
}

/**
 * this function will determine if it is safe to exit the program
 * @param v is the vector that contains all of the users
 * @param run is the boolean that runs the main program
 */
void case3(vector<PartTime>& v, bool& run)
{
    try
    {
        // check if any user is still checked in
        for(int i = 0; i < v.size(); i++)
        {
            if(v[i].getIsCheckedin())
                throw 1;
        }
        
        // export all user's data to a txt incase they haven't done so
        for(int i = 0; i < v.size(); i++)
        {
            v[i].toFile();
        }
        
        run = false;
    }
    catch(int errorCode)
    {
        cout << "ERROR: SOME USER IS STILL CHECKED IN" << endl;
        line();
    }
}