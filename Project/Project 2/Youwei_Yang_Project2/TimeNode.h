/* 
 * File:   TimeNode.h
 * Author: Stanley
 *
 * Created on December 11, 2016, 1:10 AM
 */

#ifndef TIMENODE_H
#define	TIMENODE_H
#include "Time.h"

class TimeNode
{
public:
    // Constructor
    TimeNode();
    
    // Public Variables
    Time checkinTime;
    Time checkoutTime;
    Time sessionTime;
    TimeNode* tail;
};

#endif	/* TIMENODE_H */

