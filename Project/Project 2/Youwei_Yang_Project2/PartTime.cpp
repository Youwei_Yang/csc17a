/* 
 * File:   PartTime.cpp
 * Author: Stanley
 * 
 * Created on December 5, 2016, 5:35 PM
 */

#include "PartTime.h"

/**
 * The only constructor
 * Will also define the size and isCheckedin variables
 * @param firstName the first name of the part time employee
 * @param lastName the last name of the part time employee
 * @param IDNum the ID number of the part time employee
 */
PartTime::PartTime(string firstName, string lastName,int IDNum)
: Employee(firstName, lastName, IDNum)
{
    size = -1;
    isCheckedin = false;
}

PartTime::PartTime(const PartTime& orig)
: Employee(orig)
{

}

/**
 * delete the dynamic data, sessionTime
 */
PartTime::~PartTime()
{
    sessionTime.~TimeDynamicQueue();
}

/**
 * check in the user
 * if the user is successful checked in, set isCheckedin to true
 */
void PartTime::checkin()
{
    if(isCheckedin) // if the user IS checked in, return
    {
        cout << "ERROR: ALREADY CHECKED IN" << endl;
        return;
    }
    else
    {
        size++;
        isCheckedin = true;
        checkinTime.setCurrentTime();
        cout << "YOU ARE NOW CHECKED IN" << endl;
    }
}

/**
 * check out the user
 * if the user is successful checked out, set isCheckedin to false
 */
void PartTime::checkout()
{
    if(!isCheckedin) // if the user IS NOT checked in, return
    {
        cout << "error: not checked" << endl;
        return;
    }
    else
    {
        isCheckedin = false;
        checkoutTime.setCurrentTime();
        getSessionTime();
        cout << "YOU ARE NOW CHECKED OUT" << endl;
    }
}

/**
 * this is a private function that is only used for checkout()
 * will get the total time from check in to check out then add the time to
 * the dynamic queue: sessionTime;
 * the function will then add the session time to the totalTime;
 */
void PartTime::getSessionTime()
{
    int sessionHour = 0;
    int sessionMinute = 0;
    
    sessionHour = checkoutTime.getHour() - checkinTime.getHour();
    
    if(sessionHour < 0) // if checkout hour is smaller than check in hour
    {
        sessionHour = sessionHour + 24;
    }
    else if(sessionHour == 0) // if checkout hour equals check in hour
    {
        if(checkoutTime.getDay() != checkinTime.getDay())
            sessionHour = 24;
    }
    
    sessionMinute = checkoutTime.getMinute() - checkinTime.getMinute();
    if(sessionMinute < 0)
    {
        sessionMinute = sessionMinute + 60;
        sessionHour--;
    }
    
    Time temp(sessionMinute, sessionHour);
    sessionTime.enqueue(checkinTime, checkoutTime, temp);
    
    // Add the current sessionTime to the totalTime
    totalTime.setHour(totalTime.getHour()+sessionHour);
    totalTime.setMinute(totalTime.getMinute()+sessionMinute);
    while(totalTime.getMinute() > 0)
    {
        totalTime.setMinute(totalTime.getMinute()-60);
        totalTime.setHour(totalTime.getHour()+1);
    }
}

/**
 * delete everything in the dynamic data, sessionTime
 * set the size back to -1
 * set the isCheckedin back to false
 */
void PartTime::clear()
{
    size = -1;
    isCheckedin = false;
    sessionTime.~TimeDynamicQueue();
    cout << "ALL RECORDED TIME HAS BEEN CLEARED" << endl;
}

/**
 * print all recorded time and total time
 */
void PartTime::print()
{
    sessionTime.printQueue();
    
    cout << "Total  time: " << totalTime.timeToString() << endl;
}

/**
 * export the following to an external file as a string in a txt file
 * user first and last name and ID number
 * all recorded time and total time
 */
void PartTime::toFile()
{
    string fileName = getFirstName() + "_" + getLastName() + "_TimeSheet.txt";
    ofstream ofile;
    ofile.open(fileName);
    
    ofile << firstName << " " << lastName
            << " : " << IDNum << endl << endl;
    
    while(!sessionTime.isEmpty())
    {
        string oString;
        sessionTime.dequeue(oString);
        ofile << oString;
        ofile << endl << endl;
    }
    
    ofile << "Total combined time: " << totalTime.timeToString() << endl;
    
    ofile.close();
    
    cout << "OUTPUT TO A FILE: COMPLETE" << endl;
}

/**
 * @return a boolean of  whether the user is checked in
 */
bool PartTime::getIsCheckedin()
{
    return isCheckedin;
}