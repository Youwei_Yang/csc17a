/* 
 * File:   Time.h
 * Author: Stanley
 *
 * Created on December 6, 2016, 1:09 PM
 */

#ifndef TIME_H
#define	TIME_H
#include <cstdlib>
#include <iostream>
#include <ctime>

using namespace std;

class Time
{
public:
    // Constructor
    Time();
    Time(int minute, int hour);
    Time(const Time& orig);
    virtual ~Time();
    
    // Setters && Getters
    void setMinute(int minute);
    void setHour(int hour);
    void setDay(int day);
    void setMonth(int month);
    void setYear(int year);
    int getMinute();
    int getHour();
    int getDay();
    int getMonth();
    int getYear();
    
    // Other Functions
    void setCurrentTime();
    string dateToString();
    string timeToString();
    
private:
    int minute;
    int hour;
    int day;
    int month;
    int year;
};

#endif	/* TIME_H */