/* 
 * File:   PartTime.h
 * Author: Stanley
 *
 * Created on December 5, 2016, 5:35 PM
 */

#ifndef PARTTIME_H
#define	PARTTIME_H
#include <ostream>
#include <fstream>
#include "Employee.h"
#include "TimeDynamicQueue.h"

using namespace std;

class PartTime : public Employee
{
public:
    // Constructor
    PartTime(string firstName, string lastName,int IDNum);
    PartTime(const PartTime& orig);
    virtual ~PartTime();
    
    // Other Functions
    void checkin();
    void checkout();
    void clear();
    void print();
    void toFile();
    
    // Getters && Setters
    bool getIsCheckedin();
    
private:
    Time checkinTime;
    Time checkoutTime;
    TimeDynamicQueue sessionTime;
    Time totalTime;
    bool isCheckedin;
    int size;
    
    // Private Functions
    void getSessionTime();
};

#endif	/* PARTTIME_H */