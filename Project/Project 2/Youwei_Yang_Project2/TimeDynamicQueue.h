/* 
 * File:   TimeDynamicQueue.h
 * Author: Stanley
 *
 * Created on December 11, 2016, 12:57 AM
 */

#ifndef TIMEDYNAMICQUEUE_H
#define	TIMEDYNAMICQUEUE_H
#include "TimeNode.h"

class TimeDynamicQueue
{
public:
    // Constructors
    TimeDynamicQueue();
    virtual ~TimeDynamicQueue();
    
    // Other Functions
    void enqueue(Time checkinTime, Time checkoutTime, Time sessionTime);
    void dequeue(string& x);
    void clear();
    bool isEmpty();
    void printQueue();
private:
    TimeNode* front;
    TimeNode* rear;
    int numItems;
};

#endif	/* TIMEDYNAMICQUEUE_H */

