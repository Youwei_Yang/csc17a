/* 
 * File:   Employee.cpp
 * Author: Stanley
 * 
 * Created on December 5, 2016, 1:33 PM
 */

#include "Employee.h"

/**
 * This is the only constructor
 * @param firstName the first name of the employee
 * @param lastName the last name of the employee
 * @param IDNum the ID number of the employee
 */
Employee::Employee(string firstName, string lastName, int IDNum)
{
    this->firstName = firstName;
    this->lastName = lastName;
    this->IDNum = IDNum;
}

/**
 * Copy Constructor
 * @param orig the employee object to be copy from
 */
Employee::Employee(const Employee& orig)
{
    this->firstName = orig.firstName;
    this->lastName = orig.lastName;
    this->IDNum = orig.IDNum;
}

/**
 * no dynamic data to be deleted
 */
Employee::~Employee()
{
    
}

/**
 * Change the first name of the employee object
 * @param firstName is the string that will replace the existing firstName
 */
void Employee::setFirstName(string firstName)
{
    this->firstName = firstName;
}

/**
 * Change the last name of the employee object
 * @param lastName is the string that will replace the existing lastName
 */
void Employee::setLastName(string lastName)
{
    this->lastName = lastName;
}

/**
 * Change the ID Number of the employee object
 * @param IDNum is the int that will replace the existing IDNum
 */
void Employee::setIDNum(int IDNum)
{
    this->IDNum = IDNum;
}

/**
 * @return a string of employee's first name
 */
string Employee::getFirstName()
{
    return firstName;
}

/**
 * @return a string of employee's last name
 */
string Employee::getLastName()
{
    return lastName;
}

/**
 * @return an int of employee's ID number
 */
int Employee::getIDNum()
{
    return IDNum;
}