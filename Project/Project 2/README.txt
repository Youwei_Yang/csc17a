{\rtf1\ansi\ansicpg1252\cocoartf1504\cocoasubrtf760
{\fonttbl\f0\fswiss\fcharset0 Helvetica;}
{\colortbl;\red255\green255\blue255;}
{\*\expandedcolortbl;;}
{\*\listtable{\list\listtemplateid1\listhybrid{\listlevel\levelnfc23\levelnfcn23\leveljc0\leveljcn0\levelfollow0\levelstartat1\levelspace360\levelindent0{\*\levelmarker \{disc\}}{\leveltext\leveltemplateid1\'01\uc0\u8226 ;}{\levelnumbers;}\fi-360\li720\lin720 }{\listlevel\levelnfc23\levelnfcn23\leveljc0\leveljcn0\levelfollow0\levelstartat1\levelspace360\levelindent0{\*\levelmarker \{hyphen\}}{\leveltext\leveltemplateid2\'01\uc0\u8259 ;}{\levelnumbers;}\fi-360\li1440\lin1440 }{\listlevel\levelnfc23\levelnfcn23\leveljc0\leveljcn0\levelfollow0\levelstartat1\levelspace360\levelindent0{\*\levelmarker \{hyphen\}}{\leveltext\leveltemplateid3\'01\uc0\u8259 ;}{\levelnumbers;}\fi-360\li2160\lin2160 }{\listname ;}\listid1}
{\list\listtemplateid2\listhybrid{\listlevel\levelnfc23\levelnfcn23\leveljc0\leveljcn0\levelfollow0\levelstartat1\levelspace360\levelindent0{\*\levelmarker \{disc\}}{\leveltext\leveltemplateid101\'01\uc0\u8226 ;}{\levelnumbers;}\fi-360\li720\lin720 }{\listlevel\levelnfc23\levelnfcn23\leveljc0\leveljcn0\levelfollow0\levelstartat1\levelspace360\levelindent0{\*\levelmarker \{hyphen\}}{\leveltext\leveltemplateid102\'01\uc0\u8259 ;}{\levelnumbers;}\fi-360\li1440\lin1440 }{\listname ;}\listid2}}
{\*\listoverridetable{\listoverride\listid1\listoverridecount0\ls1}{\listoverride\listid2\listoverridecount0\ls2}}
\margl1440\margr1440\vieww20720\viewh14280\viewkind0
\pard\tx720\tx1440\tx2160\tx2880\tx3600\tx4320\tx5040\tx5760\tx6480\tx7200\tx7920\tx8640\pardirnatural\partightenfactor0

\f0\fs40 \cf0 README\
\
Youwei_Yang_Project2\
\
My project is a time sheet program that will log the check in and check out time of multiple employee. The program will also be able to export the entire time sheet to a .txt file.\
\
With project 2, I was able to make couple improvements:\
\pard\tx220\tx720\tx1440\tx2160\tx2880\tx3600\tx4320\tx5040\tx5760\tx6480\tx7200\tx7920\tx8640\li720\fi-720\pardirnatural\partightenfactor0
\ls1\ilvl0\cf0 {\listtext	\'95	}The entire program have been totally rewritten for the following reasons:\
\pard\tx940\tx1440\tx2160\tx2880\tx3600\tx4320\tx5040\tx5760\tx6480\tx7200\tx7920\tx8640\li1440\fi-1440\pardirnatural\partightenfactor0
\ls1\ilvl1\cf0 {\listtext	\uc0\u8259 	}Readability\
{\listtext	\uc0\u8259 	}Logic Error\
{\listtext	\uc0\u8259 	}New Classes\
{\listtext	\uc0\u8259 	}New data structure\
{\listtext	\uc0\u8259 	}new implementation with codes\
\pard\tx220\tx720\tx1440\tx2160\tx2880\tx3600\tx4320\tx5040\tx5760\tx6480\tx7200\tx7920\tx8640\li720\fi-720\pardirnatural\partightenfactor0
\ls1\ilvl0\cf0 {\listtext	\'95	}Menu readability is improved within codes\
{\listtext	\'95	}New and improved Time( ) class that can automatically record the current time\
\pard\tx940\tx1440\tx2160\tx2880\tx3600\tx4320\tx5040\tx5760\tx6480\tx7200\tx7920\tx8640\li1440\fi-1440\pardirnatural\partightenfactor0
\ls1\ilvl1\cf0 {\listtext	\uc0\u8259 	}User no longer have to input the time them self\
\pard\tx220\tx720\tx1440\tx2160\tx2880\tx3600\tx4320\tx5040\tx5760\tx6480\tx7200\tx7920\tx8640\li720\fi-720\pardirnatural\partightenfactor0
\ls1\ilvl0\cf0 {\listtext	\'95	}Created and modified Node and Dynamic Queue\
{\listtext	\'95	}Program now stores all recored times into a dynamic queue\
\pard\tx940\tx1440\tx2160\tx2880\tx3600\tx4320\tx5040\tx5760\tx6480\tx7200\tx7920\tx8640\li1440\fi-1440\pardirnatural\partightenfactor0
\ls1\ilvl1\cf0 {\listtext	\uc0\u8259 	}Use to store within an dynamic array\
\pard\tx220\tx720\tx1440\tx2160\tx2880\tx3600\tx4320\tx5040\tx5760\tx6480\tx7200\tx7920\tx8640\li720\fi-720\pardirnatural\partightenfactor0
\ls1\ilvl0\cf0 {\listtext	\'95	}Implemented try-catch function for any error check\
{\listtext	\'95	}All users will be stored with in a vector, which allows infinite user creations\
\pard\tx940\tx1440\tx2160\tx2880\tx3600\tx4320\tx5040\tx5760\tx6480\tx7200\tx7920\tx8640\li1440\fi-1440\pardirnatural\partightenfactor0
\ls1\ilvl1\cf0 {\listtext	\uc0\u8259 	}use to be dynamic array with the size of 10\
\pard\tx220\tx720\tx1440\tx2160\tx2880\tx3600\tx4320\tx5040\tx5760\tx6480\tx7200\tx7920\tx8640\li720\fi-720\pardirnatural\partightenfactor0
\ls1\ilvl0\cf0 {\listtext	\'95	}Login has been changed to using employee ID\
\pard\tx940\tx1440\tx2160\tx2880\tx3600\tx4320\tx5040\tx5760\tx6480\tx7200\tx7920\tx8640\li1440\fi-1440\pardirnatural\partightenfactor0
\ls1\ilvl1\cf0 {\listtext	\uc0\u8259 	}Use to use first name to log in, and last name if there was any duplicated first name\
{\listtext	\uc0\u8259 	}With this change, it will be less likely to have any conflict when creating new user or login\
\pard\tx220\tx720\tx1440\tx2160\tx2880\tx3600\tx4320\tx5040\tx5760\tx6480\tx7200\tx7920\tx8640\li720\fi-720\pardirnatural\partightenfactor0
\ls1\ilvl0\cf0 {\listtext	\'95	}When create a new user, program will now check if the same employee ID already exist\
{\listtext	\'95	}When exiting the program, program will check if any user is still checked int\
\pard\tx940\tx1440\tx2160\tx2880\tx3600\tx4320\tx5040\tx5760\tx6480\tx7200\tx7920\tx8640\li1440\fi-1440\pardirnatural\partightenfactor0
\ls1\ilvl1\cf0 {\listtext	\uc0\u8259 	}If there is; return an error message, and go back to main menu\
{\listtext	\uc0\u8259 	}If there isn\'92t; export all users\'92 recorded times, incase any of them haven\'92t done so\
\pard\tx220\tx720\tx1440\tx2160\tx2880\tx3600\tx4320\tx5040\tx5760\tx6480\tx7200\tx7920\tx8640\li720\fi-720\pardirnatural\partightenfactor0
\ls1\ilvl0\cf0 {\listtext	\'95	}Couple new options have been added within the user menu:\
\pard\tx940\tx1440\tx2160\tx2880\tx3600\tx4320\tx5040\tx5760\tx6480\tx7200\tx7920\tx8640\li1440\fi-1440\pardirnatural\partightenfactor0
\ls1\ilvl1\cf0 {\listtext	\uc0\u8259 	}Review Time Sheet\
\pard\tx1660\tx2160\tx2880\tx3600\tx4320\tx5040\tx5760\tx6480\tx7200\tx7920\tx8640\li2160\fi-2160\pardirnatural\partightenfactor0
\ls1\ilvl2\cf0 {\listtext	\uc0\u8259 	}Will display all recorded time without exporting them yet\
\pard\tx940\tx1440\tx2160\tx2880\tx3600\tx4320\tx5040\tx5760\tx6480\tx7200\tx7920\tx8640\li1440\fi-1440\pardirnatural\partightenfactor0
\ls1\ilvl1\cf0 {\listtext	\uc0\u8259 	}Setting\
\pard\tx1660\tx2160\tx2880\tx3600\tx4320\tx5040\tx5760\tx6480\tx7200\tx7920\tx8640\li2160\fi-2160\pardirnatural\partightenfactor0
\ls1\ilvl2\cf0 {\listtext	\uc0\u8259 	}Leads into the new Setting menu\
\pard\tx220\tx720\tx1440\tx2160\tx2880\tx3600\tx4320\tx5040\tx5760\tx6480\tx7200\tx7920\tx8640\li720\fi-720\pardirnatural\partightenfactor0
\ls1\ilvl0\cf0 {\listtext	\'95	}Added a new Setting menu with in the user menu\
\pard\tx940\tx1440\tx2160\tx2880\tx3600\tx4320\tx5040\tx5760\tx6480\tx7200\tx7920\tx8640\li1440\fi-1440\pardirnatural\partightenfactor0
\ls1\ilvl1\cf0 {\listtext	\uc0\u8259 	}Within the Setting menu, option to change first and last name and ID Number have been added\
\pard\tx1660\tx2160\tx2880\tx3600\tx4320\tx5040\tx5760\tx6480\tx7200\tx7920\tx8640\li2160\fi-2160\pardirnatural\partightenfactor0
\ls1\ilvl2\cf0 {\listtext	\uc0\u8259 	}when changing the ID number, the program will also check if the new ID number already existed\
\pard\tx720\tx1440\tx2160\tx2880\tx3600\tx4320\tx5040\tx5760\tx6480\tx7200\tx7920\tx8640\pardirnatural\partightenfactor0
\cf0 \
Couple things that I wasn\'92t able to implement:\
\pard\tx220\tx720\tx1440\tx2160\tx2880\tx3600\tx4320\tx5040\tx5760\tx6480\tx7200\tx7920\tx8640\li720\fi-720\pardirnatural\partightenfactor0
\ls2\ilvl0\cf0 {\listtext	\'95	}Not able to import your own time sheet\
\pard\tx940\tx1440\tx2160\tx2880\tx3600\tx4320\tx5040\tx5760\tx6480\tx7200\tx7920\tx8640\li1440\fi-1440\pardirnatural\partightenfactor0
\ls2\ilvl1\cf0 {\listtext	\uc0\u8259 	}which will defeat the purpose of using this program\
{\listtext	\uc0\u8259 	}will be difficult because of the dynamic queue\
\pard\tx720\tx1440\tx2160\tx2880\tx3600\tx4320\tx5040\tx5760\tx6480\tx7200\tx7920\tx8640\pardirnatural\partightenfactor0
\cf0 \
EXTRA CREDIT\
With project 2, I decide to implement dynamic queue (use to be dynamic array) when storing all recorded times mainly because of the First In First Out (FIFO) property of the queue. Since when I store the time and export the time, I will need them to be in order they were stored, queue was the best choice. I did, however, made couple changes to the data structure. Mainly within the Node class. My Node class have 3 values instead of 1. This is because I need to store the time checked in, time checked out, and session time, which are all objects instead of integers or strings. With just 1 value within the Node class will be very difficult. I also added a new function, printQueue( ), to the dynamic queue, which simply prints out everything with in the queue without making any changes to the queue. This function is mainly used for the \'93Review Time Sheet\'94 option within the user menu. Having the function implemented to the dynamic queue just makes the coding easier.\
}