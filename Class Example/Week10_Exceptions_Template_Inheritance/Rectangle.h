/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Rectangle.h
 * Author: rcc
 *
 * Created on November 3, 2016, 7:38 PM
 */

#ifndef RECTANGLE_H
#define RECTANGLE_H

#include <fstream>;

class Rectangle
{
public:
    Rectangle();
    
    // Create the NegWidth Class
    class NegWidth {};
    class NegLength {};
    
    // Getters/Setters
    void setWidth(int width);
    void setLength(int length);
    
    friend std::ostream& operator << (std::ostream& out, const Rectangle&);
    
private:
    int width;
    int length;
};

#endif /* RECTANGLE_H */