/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Rectangle.cpp
 * Author: rcc
 * 
 * Created on November 3, 2016, 7:38 PM
 */

#include "Rectangle.h"

Rectangle::Rectangle()
{
    width = 0;
    length = 0;
}

void Rectangle::setWidth(int width)
{
    // Only assign width if it is valid
    if(width >= 0)
    {
        this->width = width;
    }
    else
        throw NegWidth();
}

void Rectangle::setLength(int length)
{
    // Only assign width if it is valid
    if(length >= 0)
    {
        this->length = length;
    }
    else
        throw NegLength();
}

std::ostream& operator << (std::ostream& out, const Rectangle& r)
{
    out << r.length << " " << r.width << std::endl;
}