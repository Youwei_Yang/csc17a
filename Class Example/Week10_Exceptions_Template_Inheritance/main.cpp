/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on November 3, 2016, 7:36 PM
 */

#include <cstdlib>
#include "Rectangle.h"
#include <iostream>

using namespace std;

// Templates
template <class T>
void print(T object)
{
    cout << "The value is: " << object << endl;
}

template <class T1, class T2>
void print2(T1 x, T2 y, Rectangle r)
{
    cout << "The value of 1: " << x << endl;
    cout << "The value of 2: " << y << endl;
}

/*
 * 
 */
int main(int argc, char** argv) {

    Rectangle rectangle;
    
    // Get user input
    int width;
    int length;
    
    cout << "Please enter a width: " << endl;
    cin >> width;
    
    cout << "Please enter a length: " << endl;
    cin >> length;
    
    // Try catch the length and width
    bool tryAgain = true;
    
    while(tryAgain)
    {
        try
        {
            rectangle.setLength(length);
            rectangle.setWidth(width);

            cout << "The width and length have been set!" << endl;
            tryAgain = false;
        }
        catch (Rectangle::NegWidth negSize)
        {
            cout << "Negative width detected!" << endl;
            
            // Get user input
            cout << "Please enter a width: " << endl;
            cin >> width;
        }
        catch (Rectangle::NegLength negLength)
        {
            cout << "Negative length detected!" << endl;
            
            // Get user input
            cout << "Please enter a length: " << endl;
            cin >> length;
        }
    }
    
    cout << "End of program." << endl;
    
    // Print a int and a double and a string
    int val = 30;
    double val2 = 2.5;
    string val3 = "Hello World";
    
    print(val);
    print(val2);
    print(val3);
    print(rectangle);
            
    return 0;
}

