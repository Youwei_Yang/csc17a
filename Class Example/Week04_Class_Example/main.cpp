/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on September 22, 2016, 7:19 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

enum candyType { CHEWY, SOUR, CHOCOLATE};

class Candy
{
private:
    string ingredientList;
    string name;
    candyType type;
public:
    // Getter
    string getName();
    
    // Setter
    void setName(string);
    
    // Default Constructor
    Candy();
    Candy(string, candyType);
};

/*
 * 
 */
int main(int argc, char** argv) {

    Candy candy; // Default constructor
    
    cout << "Enter a candy name: ";
    string name;
    cin >> name;
    
    // Change the candy name
    candy.setName(name);
    
    cout << "Candy name: " << candy.getName() << endl;
    
    return 0;
}

// Definitions
string Candy::getName()
{
    return this->name;
}

void Candy::setName(string name)
{
    this->name = name;
}

Candy::Candy()
{
    ingredientList = "";
    name = "";
    type = CHEWY;
}

Candy::Candy(string name, candyType type)
{
    this->name = name;
    this->type = type;
    ingredientList = "";
}