/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Car.h
 * Author: rcc
 *
 * Created on October 27, 2016, 8:06 PM
 */

#ifndef CAR_H
#define CAR_H

// INCLUDE VEHICLE
#include "Vehicle.h"

// Inherits from "Vehicle"
class Car : public Vehicle
{
public:
    Car();
    Car(string make, int numWheels, double engineSize);
    Car(const Car& orig);
    virtual ~Car();
private:
    string make;
    
    public:
        void setMake(string make);
        void print();
};

#endif /* CAR_H */

