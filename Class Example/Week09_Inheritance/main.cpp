/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on October 27, 2016, 7:56 PM
 */

#include <cstdlib>

#include "Car.h"

using namespace std;

/*
 * 
 */
int main(int argc, char** argv)
{
    // Create a default car
    Car c;
    c.print();
    
    Vehicle v;
    v.print();
    
    
    return 0;
}

