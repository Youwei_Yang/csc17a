/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Car.cpp
 * Author: rcc
 * 
 * Created on October 27, 2016, 8:06 PM
 */

#include "Car.h"

// Use initialization list
// Initialization list invokes the base constructor
Car::Car()
        : Vehicle()
{
    
}

// TO DO
Car::Car(const Car& orig)
{
    
}

// LEAVE EMPTY CURLY BRACE
Car::~Car()
{
    
}

void Car::setMake(string make)
{
    this->make = make;
}

Car::Car(string make, int numWheels, double engineSize)
        : Vehicle(numWheels, engineSize)
{
    this->make = make;
}

void Car::print()
{
    cout << "This is Car::print()" << endl;
    cout << "Make: " << make << endl;
    
    // using "Vehicle::" because want to print the vehicle version
    Vehicle::print();
    
}