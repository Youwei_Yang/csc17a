/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Vehicle.cpp
 * Author: rcc
 * 
 * Created on October 27, 2016, 7:57 PM
 */

#include "Vehicle.h"

Vehicle::Vehicle()
{
    numWheels = 0;
    engineSize = 0.0;
}

Vehicle::Vehicle(const Vehicle& orig)
{
    this->numWheels = orig.numWheels;
    this->engineSize = orig.engineSize;
}

// Empty curly braces is needed
Vehicle::~Vehicle()
{
    
}

Vehicle::Vehicle(int numWheels, double engineSize)
{
    this->numWheels = numWheels;
    this->engineSize = engineSize;
}

void Vehicle::setWheels(int wheels)
{
    this->numWheels = wheels;
}

void Vehicle::print()
{
    cout << "This is the Vehicle::print()" << endl;
    cout << "Num Wheels: " << numWheels << endl;
    cout << "Engine Size: " << engineSize << endl;
}