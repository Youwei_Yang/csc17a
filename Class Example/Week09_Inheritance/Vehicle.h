/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Vehicle.h
 * Author: rcc
 *
 * Created on October 27, 2016, 7:57 PM
 */

#ifndef VEHICLE_H
#define VEHICLE_H
#include <iostream>

using namespace std;

class Vehicle
{
public:
    Vehicle();
    Vehicle(int numWheels, double engineSize);
    Vehicle(const Vehicle& orig);
    virtual ~Vehicle(); // Virtual is needed for destructor
private:
    int numWheels;
    double engineSize;
public:
    // Setter
    void setWheels(int wheels);
    // Prints out a vehicle
    void print();
};

#endif /* VEHICLE_H */