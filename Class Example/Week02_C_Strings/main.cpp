/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on September 13, 2016, 6:37 PM
 */

#include <cstdlib>
#include <iostream>
#include <fstream>
#include <cctype>

using namespace std;

// Prototypes
int getNumUpper(string);
int getNumLower(string);
int getNumNumeric(string);
int getNumUpper(char[]);
int getNumLower(char[]);
int getNumNumeric(char[]);

/*
 * 
 */
int main(int argc, char** argv) {
    
    //To read fro files, 3 steps
    // Step1: Open the file
    
    string fileName = "data.txt";
    ifstream fin;
    fin.open(fileName.c_str()); //Convert to c-string. c-Strings
    // c-strings are constant
    
    // Step 2: Reading the file
    int numUpper = 0;
    int numLower = 0;
    int numDigits = 0;
    
    //Read
    string input; //using a string
    while(fin >> input) //!fin.eof(); <- end of file
    {
        numUpper += getNumUpper(input);
        numLower += getNumLower(input);
        numDigits += getNumNumeric(input);
    }
    
    cout << "The file contains: " << numUpper << " upper case." << endl;
    cout << "The file contains: " << numLower << " lower case." << endl;
    cout << "The file contains: " << numDigits << " numbers." << endl;
    
    // Step3: Close the file
    fin.close();
    
    
    /**************************************************************************/
    // C-strings
    fin.open(fileName);
    // Step 2: Reading the file
    numUpper = 0;
    numLower = 0;
    numDigits = 0;
    
    //Read
    char inputC[100]; //using a c-string
    while(fin >> inputC) //!fin.eof(); <- end of file
    {
        numUpper += getNumUpper(inputC);
        numLower += getNumLower(inputC);
        numDigits += getNumNumeric(inputC);
        //Reset
    }
    
    cout << "The file contains: " << numUpper << " upper case." << endl;
    cout << "The file contains: " << numLower << " lower case." << endl;
    cout << "The file contains: " << numDigits << " numbers." << endl;
    
    // Step3: Close the file
    fin.close();
    return 0;
}

int getNumUpper(string s)
{
    int count = 0;
    for (int i = 0; i < s.size(); i++)
    {
        if (isupper(s[i]))
        {
            count++;
        }
    }
    
    return count;
}

int getNumLower(string s)
{
    int count = 0;
    for (int i = 0; i < s.size(); i++)
    {
        if (islower(s[i]))
        {
            count++;
        }
    }
    
    return count;
}

int getNumNumeric(string s)
{
    int count = 0;
    for (int i = 0; i < s.size(); i++)
    {
        if (isdigit(s[i]))
        {
            count++;
        }
    }
    
    return count;
}

int getNumNumeric(char c[])
{
    int count = 0; // Count the number of digits
    int size = 0; // Keeps track of the array
    while(c[count] != '\0') // Checking for null terminator
    {
        if (isdigit(c[size]))
        {
            count++;
        }
        size++;
    }
    
    return count;
}

int getNumUpper(char c[])
{
    int count = 0;
    int size = 0;
    while(c[size] != '\0') // Checking for null terminator
    {
        if (isupper(c[size]))
        {
            count++;
        }
        size++;
    }
    
    return count;
}

int getNumLower(char c[])
{
    int count = 0;
    int size = 0;
    while(c[count] != '\0') // Checking for null terminator
    {
        if (islower(c[size]))
        {
            count++;
        }
        size++;
    }
    
    return count;
}