/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on October 4, 2016, 6:08 PM
 */

#include <cstdlib>

using namespace std;

class Test
{
private:
    int*tests;
    int size;
public:
    //Getter
    int getTests();
    int getSize();
    //Setter
    void setSize(int);
    void setTests(int*);
    //Constructor
    Test(); //Default
    Test(int);
    Test(const Test&); //Copy constructor
    ~Test(); //Deconstructor
    //Definition
    //Overloading
    //This changes the meaning of "=" for test datatype
    void Test::operator = (const Test&right)
    {
        this->size = right.size;
        this->tests = new int[size];
        for(int i = 0; i < size; i++)
        {
            this->tests[i] = right.tests[i];
        }
    }
    
    // Using const so nothing on the right and left side could be changed
    // using & so we don't accidently changes the right side
    const Test Test::operator = (const Test&right)
    {
        if(this == &right)
        {
            return *this;
        }
        
        delete[] this->tests;
        this->size = right.size; //Copy
        this->tests = new int[this->size];
        for(int i = 0; i < size; i++)
        {
            this->tests[i] = right.tests[i];
        }
        return *this;
    }
};

/*
 * 
 */
int main(int argc, char** argv) {
    
    Test a(5);
    Test b = a; // Using copy constructor
    // Above is an example of "Shallow-Copy"
    // b/c it only copy on the surface level, they are essentially the same address
    Test c(10);
    a = c; // Also a shallow copy
    
    //With Definition
    Test a(10);
    Test b(5);
    a = b; // Works with the "void Test::operator"
    
    Test c;
    c = b = a; // Only Works with "const Test Test::operator"
    
    Test a;
    a = a; // only works by adding "if(this == &right)"
    
    return 0;
}

int Test::getSize()
{
    return this->size;
}

void Test::setSize(int size)
{
    this->size = size;
}

void Test::setTests(int*value)
{
    this->tests = value;
}

Test::Test()
{
    size = 0;
    tests = NULL;
}

Test::Test(int size)
{
    this->size = size;
    this->tests = new int[size];
    for(int i = 0; i < size; i ++)
    {
        tests[i] = 0;
    }
}

Test::Test(const Test&right)
{
    this->size = right.size;
    this->tests = new int[this->size];
    // Copy all values
    for(int i = 0; i < size; i++)
    {
        this->tests[i] = right.tests[i];
    }
}

Test::~Test()
{
    delete[]tests;
}

//The big 3 are the getter and setter, constructor, deconstructor
// Getter: return a value from the class
// Setter: set or define a value from a class
// Constructor: When a class is created, constructor will set or define certain 
//              values in that specific class. It should also determine what will do if no 
//              value were given
// Deconstructor: Destory the object along with its values.