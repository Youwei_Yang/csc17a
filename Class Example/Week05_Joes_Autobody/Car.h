/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Car.h
 * Author: rcc
 *
 * Created on September 29, 2016, 7:42 PM
 */

/**
 *      Car class
                Make
                Model
                Year
 */

#ifndef CAR_H
#define CAR_H

#include <iostream>
#include <string>
using namespace std;

class Car
{
public:
    Car(); // Default constructor
    Car(const Car& orig); // Copy constructor
    ~Car(); // Destructor
    int getYear() const; // Add a const to the function
    void setYear(int);
    void output() const;
    void setModel(string);
    void setMake(string);
    
    friend ostream& operator<<(ostream&, const Car&);
private:
    int year;
    string model;
    string make;
};

#endif /* CAR_H */

