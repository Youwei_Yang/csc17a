/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Customer.cpp
 * Author: rcc
 * 
 * Created on September 29, 2016, 8:05 PM
 */

#include "Customer.h"

Customer::Customer()
{
    this->name = "";
    this->address = "";
    this->telephoneNum = "";
}

Customer::Customer(const Customer& orig)
{
    this->name = "";
    this->address = "";
    this->telephoneNum = "";
}

Customer::~Customer()
{
    
}

void Customer::setName(string*& name)
{
    this->name = name;
}

void Customer::setAddress(string*& address)
{
    this->address = address;
}

void Customer::setTelephoneNum(string*& telephoneNum)
{
    this->telephoneNum = telephoneNum;
}

string Customer::getName()
{
    return name;
}

string Customer::getAddress()
{
    return address;
}

string Customer::getTelephoneNum()
{
    return telephoneNume;
}

