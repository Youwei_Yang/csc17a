/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on September 29, 2016, 7:40 PM
 */

#include <cstdlib>
#include <iostream>
#include "Car.h"

using namespace std;

ostream& operator<<(ostream& out, const Car& c)
{
    out << "Year: " << c.year << endl;
    cout << "Model: " << c.model << endl;
    cout << "Make: " << c.make << endl;
    return out;
}

/*
 * 
 */
int main(int argc, char** argv) {

    
    // Create a car object
    Car c;
    c.setYear(2000);
    
    c.output();
    
    cout << endl;
    
    //Use the overloaded ostream function
    cout << c;
    
    return 0;
}

