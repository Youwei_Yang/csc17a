/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Car.cpp
 * Author: rcc
 * 
 * Created on September 29, 2016, 7:42 PM
 */

#include "Car.h"

Car::Car()
{
    this->year = 1900;
}

Car::Car(const Car& orig)
{
    this->year = orig.year;
}

// No new data. No code needed
Car::~Car()
{
    
}

int Car::getYear() const
{
    return this->year;
}

void Car::setYear(int year)
{
    this->year = year;
}

void Car::output() const
{
    // Print out the contents of the car
    cout << "Year: " << this->year << endl;
    cout << "Make: " << this->make << endl;
    cout << "Model: " << this->model << endl;
}

void Car::setModel(string model)
{
    this->model = model;
}

void Car::setMake(string make)
{
    this->make = make;
}