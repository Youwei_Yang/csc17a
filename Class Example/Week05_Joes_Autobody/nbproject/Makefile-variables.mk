#
# Generated - do not edit!
#
# NOCDDL
#
CND_BASEDIR=`pwd`
CND_BUILDDIR=build
CND_DISTDIR=dist
# Debug configuration
CND_PLATFORM_Debug=Cygwin-Windows
CND_ARTIFACT_DIR_Debug=dist/Debug/Cygwin-Windows
CND_ARTIFACT_NAME_Debug=week05_joes_autobody
CND_ARTIFACT_PATH_Debug=dist/Debug/Cygwin-Windows/week05_joes_autobody
CND_PACKAGE_DIR_Debug=dist/Debug/Cygwin-Windows/package
CND_PACKAGE_NAME_Debug=week05joesautobody.tar
CND_PACKAGE_PATH_Debug=dist/Debug/Cygwin-Windows/package/week05joesautobody.tar
# Release configuration
CND_PLATFORM_Release=Cygwin-Windows
CND_ARTIFACT_DIR_Release=dist/Release/Cygwin-Windows
CND_ARTIFACT_NAME_Release=week05_joes_autobody
CND_ARTIFACT_PATH_Release=dist/Release/Cygwin-Windows/week05_joes_autobody
CND_PACKAGE_DIR_Release=dist/Release/Cygwin-Windows/package
CND_PACKAGE_NAME_Release=week05joesautobody.tar
CND_PACKAGE_PATH_Release=dist/Release/Cygwin-Windows/package/week05joesautobody.tar
#
# include compiler specific variables
#
# dmake command
ROOT:sh = test -f nbproject/private/Makefile-variables.mk || \
	(mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk)
#
# gmake command
.PHONY: $(shell test -f nbproject/private/Makefile-variables.mk || (mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk))
#
include nbproject/private/Makefile-variables.mk
