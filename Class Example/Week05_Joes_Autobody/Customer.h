/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Customer.h
 * Author: rcc
 *
 * Created on September 29, 2016, 8:05 PM
 */

#ifndef CUSTOMER_H
#define CUSTOMER_H

#include <iostream>
#include <string>
using namespace std;

class Customer {
public:
    Customer();
    Customer(const Customer& orig);
    virtual ~Customer();
    // Setter
    void setName(string*&);
    void setAddress(string*&);
    void setTelephoneNum(string*&);
    // Getter
    string getName();
    string getAddress();
    string getTelephoneNum();
private:
    string*name;
    string*address;
    string*telephoneNum;

};

#endif /* CUSTOMER_H */

