/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Student.cpp
 * Author: rcc
 * 
 * Created on October 6, 2016, 7:42 PM
 */

#include "Student.h"
#include <string>
#include <iostream>

Student::Student()
{
    cout << "This is the default constructor" << endl;
    name = "";
    id = 0;
    testSize = 0;
    tests = NULL;
}

Student::Student(const Student& orig)
{
    cout << "This is the copy constructor" << endl;
    // COPY VALUES OVER
    this->name = orig.name;
    this->id = orig.id;
    this->testSize = orig.testSize;
    this->tests = new int[orig.testSize];
    
    // Copy over all internal array values
    for(int i = 0; i < orig.testSize; i++)
    {
        this->tests[i] = orig.testSize;
    }
}

// Constructor that recieves an int
Student::Student(int size)
{
    testSize = size;
    id = 0;
    tests = new int[testSize];
    name = "";
    // Store default values
    for(int i = 0; i < testSize; i++)
    {
        tests[i] = 0;
    }
}

Student::~Student()
{
    cout << "This is the destructor" << endl;
    delete[] tests;
}

const Student& Student::operator =(const Student& right)
{
    cout << "This is the assignment operator overload" << endl;
    // To check if the "right" is the same as the current
    if(this == &right)
    {
        return *this;
    }
    
    // Delete old dynamic data
    delete[] tests;
    
    // Copy over right's data
    this->name = right.name;
    this->id = right.id;
    this->testSize = right.testSize;
    
    // Create a new dynamic array for value
    this->tests = new int[testSize];
    
    // Copy over all internal array data
    for(int i = 0; i < testSize; i++)
    {
        this->tests[i] = right.tests[i];
    }
    
    return *this; // Return actual object
}

// Postfix operator
Student Student::operator++(int)
{
    cout << "Postfix operator" << endl;
    // Create a dummy
    Student temp(*this);
    ++id;
    return temp;
}

// Prefix operator
Student Student::operator++()
{
    cout << "Prefix operator" << endl;
    
    ++id;
    return *this; // Return itself
}

// Subscript operator overload
int& Student::operator [](int loc)
{
    cout << "Subscript operator overloading" << endl;
    
    // Error check on subscript
    if (loc < 0 || loc >= testSize)
    {
        cout << "Out of bounds error" << endl;
        exit(0);
    }
    
    return tests[loc];
}

ostream & operator <<(ostream& out, const Student& right)
{
    out << "Name: " << right.name << endl;
    out << "ID: " << right.id << endl;
    out << "Test scores: " << endl;
    
    for(int i = 0; i < right.testSize; i++)
    {
        out << right.tests[i] << " ";
    }
    cout << endl << endl;
}