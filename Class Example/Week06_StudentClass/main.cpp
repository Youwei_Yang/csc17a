/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on October 6, 2016, 7:42 PM
 */

#include <cstdlib>
#include "Student.h"
#include <iostream>
#include <fstream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    // Create a single student
    Student a; // Default constructor
    
    // Use copy constructor
    Student b = a;
    
    Student c(4);
    
    c++;
    
    // Modify c;
    c[0] = 100; // First test score is 100
    
    // Output C
    cout << c;
    
    // Output A
    cout << endl << a;
    
    // Output Student C to a file
    // Use fstream for files
    // Create file object first
    ofstream outfile;
    
    // STEP 1: Open file
    // adding append flag, which will add new datas to the file, instead of replacing the,
    outfile.open("StudentC.txt", ios::app);
    
    // STEP 2: Write to file
    outfile << c << endl; // able to do this because of the ostream in the Student class
    outfile << a << endl;
    
    // STEP 3: Close the file
    outfile.close();
    
    return 0;
}

