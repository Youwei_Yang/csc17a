/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Student.h
 * Author: rcc
 *
 * Created on October 6, 2016, 7:42 PM
 */

#ifndef STUDENT_H
#define STUDENT_H

#include <string>

using namespace std;

class Student
{
public:
    
    // BIG 3
    Student(); // Default Constructor
    Student(const Student& orig); // Copy Constructor
    Student(int size);
    ~Student(); // Destructor
    
    // Operator overloading
    // Assignment
    const Student& operator=(const Student&);
    
    // Postfix operator
    Student operator++(int);
    
    // Prefix operator
    Student operator++();
    
    // Subscript overloading
    int& operator[](int loc);
    
    // Getter/ Setters
    string getName();
    void setName(string);
    
    int getID();
    void setID(int);
    
    // Output ostream overloading
    friend ostream& operator<<(ostream&, const Student&);
    // Overloading STREAM operators needs to be a "friend"
    // The buffer is constantly modified. Need to reference
    // Ex: cout << "Hello World" << endl;
    
    // INPUT
    friend istream& operator>>(istream&, const Student&);
    
private:
    string name;
    int id;
    int* tests;
    int testSize;
};

#endif /* STUDENT_H */

