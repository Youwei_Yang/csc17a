/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on October 13, 2016, 7:38 PM
 */

#include <cstdlib>
#include <fstream> // Files
#include <iostream>

using namespace std;

const int NAME_SIZE = 51;

int main(int argc, char** argv) {

    // Create structure
    struct InventoryItem
    {
        char name[NAME_SIZE];
        int id;
        double price;
    };
    
    cout << "Would you like to regenerate the file?";
    string input;
    cin >> input;
    if(tolower(input[0]) == 'y')
    {
        // Like a default constructor for structs
        InventoryItem item = {"", 0, 0.0};

        // Write to a file in BINARY format
        fstream file;
        file.open("data.dat", ios::out);

        for(int i = 0; i < 4; i++)
        {
            file.write(reinterpret_cast<char*>(&item), sizeof(item));
        }

        //CLOSE THE FILE
        file.close();
    }
    
    // Read each item form the file
    // Open the file
    fstream file;
    file.open("data.dat", ios::in | ios::out | ios::binary);
    
    // ERROR CHECKING
    if(!file)
    {
        cout << "File failed to open!" << endl;
        exit(1);
    }
    
    // Read first item
    InventoryItem record;
    
    // Read from a binary file
    file.read(reinterpret_cast<char*>(&record), sizeof(record));
    
    // Loop through the file until all content is read
    while(!file.eof()) // EOF WILL BE SET
    {
        // Output the contents of record
        cout << "Name of item: " << record.name << endl;
        cout << "ID: " << record.id << endl;
        cout << "Price: " << record.price << endl;
        cout << endl;
        
        file.read(reinterpret_cast<char*>(&record), sizeof(record));
    }
    
    // Ask the user if they would like to modify a single record
    cout << "Would you like to modify a record?" << endl;
    cin >> input;
    
    file.clear();
    while(tolower(input[0]) == 'y')
    {
        cout << "Enter the item to modify" << endl;
        int num;
        cin >> num;
        
        // num is the item they want to change
        file.seekg(num * sizeof(record), ios::beg);
        
        // READ ITEM TO THE USER
        file.read(reinterpret_cast<char*>(&record), sizeof(record));
        cout << "Name of item: " << record.name << endl;
        cout << "ID: " << record.id << endl;
        cout << "Price: " << record.price << endl;
        cout << endl;
        
        //Get values for each attribute
        cout << "Enter a new name" << endl;
        cin >> record.name;
        cout << "Enter a new id" << endl;
        cin >> record.id;
        cout << "Enter a new price" << endl;
        cin >> record.price;
        
        // Move over write position
        file.seekp(num * sizeof(record), ios::beg);
        file.write(reinterpret_cast<char*>(&record), sizeof(record));
        
        // Ask the user if they would like to write again
        cout << "Would you like to change another item?" << endl;
        cin >> input;
        
        file.clear();
    }
    
    // Print everything to the user
    // Move read position to start
    file.seekg(0L, ios::beg);
    
    file.read(reinterpret_cast<char*>(&record), sizeof(record));
    
    // Reset EOF flag
    file.clear();
    
    while(!file.eof()) // EOF IS A FLAG
    {
        // Output the contents of record
        cout << "Name of item: " << record.name << endl;
        cout << "ID: " << record.id << endl;
        cout << "Price: " << record.price << endl;
        cout << endl;
        
        file.read(reinterpret_cast<char*>(&record), sizeof(record));
    }
    
    // CLOSE THE FILE
    file.close();
    
    return 0;
}

