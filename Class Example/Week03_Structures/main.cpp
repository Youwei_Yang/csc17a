/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on September 15, 2016, 7:05 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

struct Student
{
    string name;
    int ids;
};

struct Classroom
{
    int desks;
    Student student;
    int location;
};

void output(Student s)
{
    cout << "Student name: " << s.name << endl;
    cout << "Student id: " << s.ids << endl;
}

void output(const Classroom & c) // Pass by reference to avoid copies of classroom
{
    cout << "Classroom # desks: " << c.desks << endl;
    output(c.student); // Output the student portion
    cout << "Classroom location: " << c.location << endl;
}

/*
 * 
 */
int main(int argc, char** argv) {

    cout << "Please enter student information!" << endl;
    cout << "Enter student name: ";
    
    string studentName;
    cin >> studentName;
    
    cout << "Enter student id: ";
    
    int id;
    cin >> id;
    
    // Create an instance of a classroom
    Classroom be208;
    
    // Store the name and ID of the student in the classroom
    be208.student.name = studentName;
    be208.student.ids = id;
    
    output(be208);
    
    // Create a set of classrooms
    int classSize = 10;
    Classroom building[classSize];
    
    // First building is original classroom
    building[0] = be208;
    
    // Output every classroom
    for(int i = 0; i < classSize; i++)
    {
        output(building[i]);
    }
    
    return 0;
}

